;;;; dodolog, aka yet another Common LISP blog
;;;;
;;;; Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
;;;;
;;;; This program is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation; either version 2 of
;;;; the License, or (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public
;;;; License along with this program; if not, write to the Free
;;;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;;;; MA 02111-1307, USA.

(in-package #:dodolog)

(setq *dispatch-table*
      (nconc
       (build-regex-dispatchers '(("\\/?$" main-page)
				  ("\\/new-post\\/?$" new-post-page)
				  ("\\/login\\/?$" login-page)
				  ("\\/logout\\/?$" logout-page)))
       (build-groups-bind-regex-dispatchers
	(("^\\/blog\\/archive\\/(\\d{4})\\/(\\d{1,2})\\/(\\d{1,2})\\/([^\\/]*)\\/?$"
	  (('parse-integer year month day) ('url-decode post-id)) single-post-page)
	 ("^\\/blog\\/archive\\/(\\d{4})\\/(\\d{1,2})\\/?$"
	  (('parse-integer year month)) date-archive-page)
	 ("^\\/blog\\/categories\\/([^\\/]*)\\/?$"
	  (('url-decode category-name)) category-archive-page)
	 ("^\\/blog\\/archive\\/(\\d{4})\\/(\\d{1,2})\\/(\\d{1,2})\\/([^\\/]*)\\/comments\\/(\\d+)\\/?$"
	  (('parse-integer year month day) ('url-decode post-id) ('parse-integer id)) comment-page)
	 ("^\\/blog\\/archive\\/(\\d{4})\\/(\\d{1,2})\\/(\\d{1,2})\\/([^\\/]*)\\/comments\\/(\\d+)\\/delete\\/?$"
	  (('parse-integer year month day) ('url-decode post-id) ('parse-integer id)) delete-comment-page)
	 ("^\\/blog\\/archive\\/(\\d{4})\\/(\\d{1,2})\\/(\\d{1,2})\\/([^\\/]*)\\/edit\\/?$"
	  (('parse-integer year month day) ('url-decode post-id)) edit-post-page)
	 ("^\\/blog\\/archive\\/(\\d{4})\\/(\\d{1,2})\\/(\\d{1,2})\\/([^\\/]*)\\/delete\\/?$"
	  (('parse-integer year month day) ('url-decode post-id)) delete-post-page)))
       (list #'default-dispatcher)))

(defun start-blog ()
  "Start the blog process"
  (setf tbnl:*tbnl-port* *dodolog-http-port*)
  (setf tbnl:*default-content-type* "text/html")
  (setf tbnl:*default-handler* '404-page)
  (setf tbnl:*session-cookie-name* "dodolog-session")
  (start-tbnl))

(defun main-page ()
  "Main blog page"
  (tmpl-show-main-page))

(defun login-page ()
  "Login page"
  (if (eq :post (request-method :request *request*))
      (if (post-parameter "cancel")
	  (redirect *dodolog-http-path-prefix*)
	  (let* ((user-nick (post-parameter "user-nick" *request*))
		 (password (post-parameter "password" *request*))
		 (errors (validate-login user-nick password)))
	    (if (not errors)
		(let* ((user (auth-get-user user-nick password)))
		  (if user
		      (progn
			(auth-login-user user)
			(let ((came-from (post-parameter "came-from"
							 *request*)))
			  (if came-from
			      (redirect came-from)
			      (redirect *dodolog-http-path-prefix*))))
		      (tmpl-show-login-page :dodolog-error
					     "Authentication failed"
					    :came-from
					     (post-parameter "came-from"
							     *request*))))
		(tmpl-show-login-page :errors errors ; Validation failed
				      :request *request*
				      :came-from (post-parameter "came-from"
								 *request*)))))
      (tmpl-show-login-page :came-from (get-parameter "came-from" *request*))))

(defun logout-page ()
  "Logout page"
  (if (eq :post (request-method :request *request*))
    (if (post-parameter "cancel")
	(redirect *dodolog-http-path-prefix*)
	(progn
	  (auth-logout-user)
	  (tmpl-show-logout-page)))
    (tmpl-show-logout-page)))
      
(defun single-post-page (&key post-id year month day)
  "Single post page"
  (with-post-with-comments-count (post year month day post-id)
    (if (eq :post (request-method :request *request*))
	(-manage-comment :post post :request *request*
			 :tmpl-page-fun #'tmpl-show-single-post-page)
	(tmpl-show-single-post-page post))))
  
(defun 404-page (&key (message nil))
  "404 error page"
  (setf (return-code) tbnl:+http-not-found+)
  (tmpl-show-404-page :message (if message message "Resource not found")))

(defun category-archive-page (&key category-name)
  "Archive page, by category"
  (let ((start (parse-integer (or (get-parameter "start" *request*)
				  "0"))))
    (tmpl-show-category-archive (db-get-category-with-posts-count
				 category-name)
				:start start)))

(defun date-archive-page (&key year month)
  "Archive page, by date"
  (let ((start (parse-integer (or (get-parameter "start" *request*)
				  "0"))))
    (tmpl-show-date-archive year month
			    :start (if (null start) 0 start))))

(defun new-post-page ()
  "Insert new post"
  (with-authenticated-user (user)
    (-manage-edit-post user (make-instance 'post))))

(defun edit-post-page (&key year month day post-id)
  "Edit post page"
  (with-authenticated-user-post (post year month day post-id)
    (-manage-edit-post (auth-get-authenticated-user) post)))

(defun delete-post-page (&key year month day post-id)
  "Post deletion page"
  (with-authenticated-user-post (post year month day post-id)
    (let ((title (title post)))
      (if (eq :post (request-method :request *request*))
	  (if (post-parameter "cancel")
	      (redirect *dodolog-http-path-prefix*)
	      (progn
		(db-delete post)
		(tmpl-show-post-delete-page :post-id post-id
					    :post-title title
					    :post-not-yet-deleted nil)))
	  (tmpl-show-post-delete-page :post-id post-id :post-title title)))))

(defun comment-page (&key year month day post-id id)
  "Page for showing a specific comment for a post"
  (with-post (post year month day post-id)
    (with-comment (comment (post id))
      (if (eq :post (request-method :request *request*))
	  (-manage-comment
	   :post post :request *request* :replied-comment comment
	   :tmpl-page-fun #'tmpl-show-comment-page)
	  (tmpl-show-comment-page post :comment comment)))))

(defun delete-comment-page (&key year month day post-id id)
  "Comment deletion page"
  (with-authenticated-user-post (post year month day post-id)
    (with-comment (comment (post id))
      (if (eq :post (request-method :request *request*))
	  (if (post-parameter "cancel")
	      (redirect (format nil "~A/archive/~D/~D/~D/~A"
				     *dodolog-http-path-prefix*
				     year month day post-id))
	      (progn
		(let ((replies-action (post-parameter "replies-action"
						      *request*)))
		  (db-delete
		   comment
		   :replies-action (cond ((string-equal replies-action
							"empty-comment")
					  :empty-comment)
					 ((string-equal replies-action
							"reparent")
					  :reparent-replies)
					 (t :cascade-replies))))
		(tmpl-show-comment-delete-page comment
					     :comment-not-yet-deleted-p nil)))
	  (tmpl-show-comment-delete-page comment)))))

(defun -manage-edit-post (user post)
  "Manage the editing of a post, either for new or existing ones"
  (if (eq :post (request-method :request *request*))
    (if (post-parameter "cancel")
      (redirect *dodolog-http-path-prefix*)
      (let ((original-id (id post)))
	(setf (user-nick post) (nick user))
	(setf (id post) (post-parameter "id" *request*))
	(setf (title post) (post-parameter "title" *request*))
	(setf (body post) (post-parameter "body" *request*))
	(let* ((selected-category-names (tmpl-get-selected-category-names
					 *request*))
	       (new-category-names (tmpl-get-new-category-names *request*))
	       (errors (validate-post post
				      selected-category-names
				      new-category-names
				      :original-id original-id)))
	  (cond ((and (post-parameter "preview") (null errors))
		 ;; Preview page
		 (setf (category-names post)
		       (concatenate 'list
				    new-category-names
				    selected-category-names))
		 (tmpl-show-post-preview-page
		  post
		  :selected-category-names selected-category-names
		  :new-category-names new-category-names
		  :new-categories-raw (post-parameter "new-categories"
						      *request*)))
		((and (post-parameter "publish") (null errors))
		 ;; Post publishing
		 (db-persist post
			     :original-id original-id
			     :selected-category-names selected-category-names
			     :new-category-names new-category-names)
		 (multiple-value-bind (year month day)
		     (year-month-day post)
		   (redirect (format nil "~A/archive/~D/~D/~D/~A"
				     *dodolog-http-path-prefix*
				     year month day (id post)))))
		(t
		 ;; Validation errors detected, let's show them
		 (tmpl-show-edit-post-page post
					   :errors errors
					   :selected-category-names
					    selected-category-names
					   :new-categories-raw
					   (post-parameter "new-categories"
							   *request*)))))))
    (tmpl-show-edit-post-page post
			      :selected-category-names (category-names post))))

(defun -manage-comment (&key request post tmpl-page-fun
			(replied-comment nil))
  "Manage the replying to a post"
  (multiple-value-bind (author email url title body)
      (-cleanup-comment *request*)
    (let ((errors (validate-comment author email url title body)))
      (cond ((and (post-parameter "publish-comment") (not errors))
	     (let ((comment (make-instance
			     'comment
			     :post-id (id post) :author author :email email
			     :url url :title title :body body
			     :ip-address (real-remote-addr request)
			     :in-reply-to (when replied-comment
					    (id replied-comment)))))
	       (db-persist comment)
	       (-notify-new-comment comment)
	       (multiple-value-bind (year month day)
		   (year-month-day post)
		 (redirect (format nil "~A/archive/~D/~D/~D/~A"
				   *dodolog-http-path-prefix*
				   year month day (id post))))))
	    ((post-parameter "publish-comment" request)
	     ;; Show validation errors
	     (funcall tmpl-page-fun
		      post
		      :comment replied-comment
		      :errors errors
		      :comment-author (post-parameter "comment-author"
						      *request*)
		      :comment-email (post-parameter "comment-email"
						     *request*)
		      :comment-url (post-parameter "comment-url" *request*)
		      :comment-title (post-parameter "comment-title"
						     *request*)
		      :comment-body (post-parameter "comment-body"
						    *request*)))
	    (t
	     ;; Uh?
	     (funcall tmpl-page-fun :post post :comment replied-comment))))))

(defun -cleanup-comment (request)
  "Cleanup comment data from the request, returning multiple values"
  (let ((author (-trim-post-parameter "comment-author" request))
	(email (-trim-post-parameter "comment-email" request))
	(url (-trim-post-parameter "comment-url" request))
	(title (-trim-post-parameter "comment-title" request))
	(body (remove #\Return (-trim-post-parameter "comment-body" request))))
    (values (nil-if-empty author)
	    (nil-if-empty email)
	    (nil-if-empty url)
	    (nil-if-empty title)
	    (nil-if-empty body))))

(defun -trim-post-parameter (param request &key (charbag +form-whitespaces+))
  "Return a trimmed version of the given post parameters from the request"
  (let ((p (post-parameter param request)))
    (when p
      (string-trim charbag p))))

(defun -notify-new-comment (comment)
  "Send an email for notifying a post owner about a new comment"
  (let* ((post (post comment))
	 (user (user post)))
    (net.post-office:send-letter
     *dodolog-mail-host*
     *dodolog-mail-from-field*
     (email user)
     (tmpl-show-comment-notification-email comment)
     :subject (concatenate
	       'string
	       *dodolog-mail-subject-prefix*
	       "New comment")
     :headers (list
	       "Content-Type: text/plain; charset=\"utf-8\""))))
