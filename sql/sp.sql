-- dodolog, aka yet another Common LISP blog
--
-- Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 2 of
-- the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program; if not, write to the Free
-- Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
-- MA 02111-1307, USA.

START TRANSACTION;

-- Get an ordered list of comments, sorted by reply, with a depth attribute
-- that allows to easily show a tree structure
CREATE OR REPLACE FUNCTION dodolog_get_comments_tree_sp (
    VARCHAR(255), -- Post id,
    INTEGER,      -- Parent comment
    INTEGER       -- Current depth
) RETURNS SETOF dodolog_comment AS '
    declare
        _post_id       ALIAS FOR $1;
        _parent_id     ALIAS FOR $2;
        _current_depth ALIAS FOR $3;

        _retval        RECORD;
        _retval_rec    RECORD;
    begin
        for _retval in SELECT id,
                              post_id,
                              timestamp,
                              user_nick,
                              author,
                              email,
                              url,
                              title,
                              body,
                              in_reply_to,
                              ip_address,
                              isdeleted,
                              _current_depth AS depth
                         FROM dodolog_comment
                        WHERE (((_post_id IS NOT NULL)
                                AND (post_id = _post_id))
                               OR TRUE)
                          AND (((_parent_id IS NULL)
                                AND (in_reply_to IS NULL))
                               OR (in_reply_to = _parent_id))
                        ORDER BY post_id, timestamp, id loop
            return next _retval;

            for _retval_rec in SELECT *
                                 FROM dodolog_get_comments_tree_sp(
                                          _retval.post_id,
                                          _retval.id,
                                          _current_depth + 1) loop
                return next _retval_rec;
            end loop;
        end loop;

        return;
    end;
' LANGUAGE plpgsql;

COMMIT TRANSACTION;
