-- dodolog, aka yet another Common LISP blog
--
-- Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 2 of
-- the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program; if not, write to the Free
-- Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
-- MA 02111-1307, USA.

START TRANSACTION;

-- Ordered posts view
CREATE VIEW dodolog_ordered_posts_view
    AS (SELECT *
          FROM dodolog_post
         ORDER BY timestamp DESC);

-- Posts with comments count
CREATE VIEW dodolog_posts_with_comments_count_view
    AS (SELECT dodolog_post.*,
               COUNT(dodolog_comment) AS comments_count
          FROM dodolog_post
          LEFT OUTER JOIN dodolog_comment
                       ON (dodolog_post.id
                           = dodolog_comment.post_id)
         GROUP BY dodolog_post.id,
                  dodolog_post.user_nick,
                  dodolog_post.timestamp,
                  dodolog_post.title,
                  dodolog_post.body);

-- Ordered posts view, with comments count
CREATE VIEW dodolog_ordered_posts_with_comments_count_view
    AS (SELECT * FROM dodolog_posts_with_comments_count_view
         ORDER BY dodolog_posts_with_comments_count_view.timestamp DESC);

-- Category archive, with comments count
CREATE VIEW dodolog_ordered_category_archive_view
    AS (SELECT dodolog_ordered_posts_with_comments_count_view.*,
               dodolog_post_category.category_name
          FROM dodolog_post_category
          INNER JOIN dodolog_ordered_posts_with_comments_count_view
                       ON (dodolog_post_category.post_id
                           = dodolog_ordered_posts_with_comments_count_view.id)
         ORDER BY dodolog_post_category.category_name,
	        dodolog_ordered_posts_with_comments_count_view.timestamp DESC);

-- Date archive, with comments count
CREATE VIEW dodolog_ordered_date_archive_view
    AS (SELECT *,
               CAST(EXTRACT('year' FROM timestamp) AS INTEGER) AS year,
               CAST(EXTRACT('month' FROM timestamp) AS INTEGER) AS month
          FROM dodolog_ordered_posts_with_comments_count_view);

-- Years and months for the date archive
CREATE VIEW dodolog_archive_dates_view
    AS (SELECT DISTINCT year, month
          FROM dodolog_ordered_date_archive_view
         ORDER BY year DESC, month DESC);

-- Posts count for months and years in the date archive
CREATE VIEW dodolog_archive_dates_with_posts_count_view
    AS (SELECT year, month, COUNT(*) AS count
          FROM dodolog_ordered_date_archive_view
         GROUP BY year, month
         ORDER BY year, month);

-- Ordered categories view
CREATE VIEW dodolog_ordered_categories_view
    AS (SELECT *
          FROM dodolog_category
         ORDER BY name);

-- Ordered categories view, with posts count
CREATE VIEW dodolog_ordered_categories_with_posts_count_view
    AS (SELECT dodolog_category.*,
               COUNT(dodolog_post_category) AS count
          FROM dodolog_category
	  LEFT OUTER JOIN dodolog_post_category
                       ON (dodolog_category.name
                           = dodolog_post_category.category_name)
         GROUP BY dodolog_category.name,
                  dodolog_category.description
         ORDER BY dodolog_category.name);

-- Comments trees
CREATE VIEW dodolog_comments_tree_view
    AS (SELECT * FROM dodolog_get_comments_tree_sp(NULL, NULL, 0));

-- Ordered posts-categories view
CREATE VIEW dodolog_ordered_post_categories_view
    AS (SELECT dodolog_post_category.post_id,
               dodolog_post_category.category_name AS name
          FROM dodolog_post_category
         ORDER BY name);

-- Ordered categories-posts view
CREATE VIEW dodolog_ordered_category_posts_view
    AS (SELECT dodolog_post_category.category_name,
               dodolog_posts_with_comments_count_view.*
          FROM dodolog_post_category
         INNER JOIN dodolog_posts_with_comments_count_view
                 ON (dodolog_post_category.post_id
                     = dodolog_posts_with_comments_count_view.id)
         ORDER BY dodolog_posts_with_comments_count_view.timestamp);

COMMIT TRANSACTION;
