-- dodolog, aka yet another Common LISP blog
--
-- Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 2 of
-- the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program; if not, write to the Free
-- Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
-- MA 02111-1307, USA.

START TRANSACTION;

-- Users
CREATE TABLE dodolog_user (
       nick      VARCHAR(255) NOT NULL PRIMARY KEY,
       fullname  VARCHAR(255) NOT NULL,
       email     VARCHAR(255) NOT NULL,
       password  VARCHAR(255) NOT NULL
);


-- Posts
CREATE TABLE dodolog_post (
       id        VARCHAR(255) NOT NULL PRIMARY KEY,
       user_nick VARCHAR(255) NOT NULL
           REFERENCES dodolog_user(nick)
           ON UPDATE CASCADE
           ON DELETE CASCADE,
       timestamp TIMESTAMP WITHOUT TIME ZONE NOT NULL
           DEFAULT current_timestamp,
       title     VARCHAR(1000) NOT NULL,
       body      TEXT NOT NULL
);

CREATE INDEX dodolog_post_timestamp_idx
    ON dodolog_post(timestamp);


-- Post-category relation
CREATE TABLE dodolog_post_category (
       post_id   VARCHAR(255) NOT NULL
           REFERENCES dodolog_post(id)
           ON UPDATE CASCADE
           ON DELETE CASCADE,
       category_name VARCHAR(255) NOT NULL,

       CONSTRAINT dodolog_post_category_unique_ck
           UNIQUE (post_id, category_name)
);


-- Categories
CREATE TABLE dodolog_category (
       name        VARCHAR(255) NOT NULL PRIMARY KEY,
       description TEXT NOT NULL DEFAULT ''
);


-- Updates to published posts
CREATE TABLE dodolog_post_update (
       id        SERIAL NOT NULL PRIMARY KEY,
       post_id   VARCHAR(255) NOT NULL
           REFERENCES dodolog_post(id)
           ON UPDATE CASCADE
           ON DELETE CASCADE,
       timestamp TIMESTAMP WITHOUT TIME ZONE UNIQUE NOT NULL
           DEFAULT current_timestamp,
       comment   VARCHAR(1000) NOT NULL
);


-- Comments for posts
CREATE TABLE dodolog_comment (
       id          SERIAL PRIMARY KEY,
       post_id     VARCHAR(255) NOT NULL
           REFERENCES dodolog_post(id)
           ON UPDATE CASCADE
           ON DELETE CASCADE,
       timestamp   TIMESTAMP WITHOUT TIME ZONE NOT NULL
           DEFAULT CURRENT_TIMESTAMP,
       user_nick    VARCHAR(255)
           REFERENCES dodolog_user(nick)
           ON UPDATE CASCADE
           ON DELETE SET NULL,
       author      VARCHAR(255),
       email       VARCHAR(50),
       url         VARCHAR(255),
       title       VARCHAR(255),
       body        VARCHAR(8192) NOT NULL,
       in_reply_to INTEGER
           REFERENCES dodolog_comment(id)
           ON UPDATE CASCADE
           ON DELETE CASCADE,

       ip_address  INET NOT NULL,

       isdeleted   BOOLEAN NOT NULL DEFAULT FALSE,

       depth       INTEGER DEFAULT NULL, -- Dummy, used by stored procedures

       CONSTRAINT dodolog_comment_user_or_author_ck
            CHECK ((user_nick = NULL)
                   OR ((author = NULL) AND (email = NULL)
                       AND (url = NULL)))
);

CREATE INDEX dodolog_comment_post_id_idx
    ON dodolog_comment(post_id);
CREATE INDEX dodolog_comment_timestamp_idx
    ON dodolog_comment(timestamp);
CREATE INDEX dodolog_comment_in_reply_to_idx
    ON dodolog_comment(in_reply_to);
CREATE INDEX dodolog_comment_isdeleted_idx
    ON dodolog_comment(isdeleted);

COMMIT TRANSACTION;
