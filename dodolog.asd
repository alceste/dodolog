;;;; dodolog, aka yet another Common LISP blog
;;;;
;;;; Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
;;;;
;;;; This program is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation; either version 2 of
;;;; the License, or (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public
;;;; License along with this program; if not, write to the Free
;;;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;;;; MA 02111-1307, USA.

(in-package #:cl-user)

(defpackage #:dodolog-system
  (:use #:asdf #:cl))

(in-package #:dodolog-system)

(defsystem dodolog
    :name "dodolog"
    :author "Alceste Scalas"
    :licence "GNU GPL"
    :description "Yet another Common LISP blog engine"
    :depends-on (#:cl-base64 #:cl-ppcre #:clsql #:html-template
		 #:ironclad #:kmrcl #:postoffice #:split-sequence #:tbnl)

    :components ((:file "auth"
			:depends-on ("packages"
				     "parameters"
				     "db"))
		 (:file "db"
			:depends-on ("packages"
				     "parameters"
				     "utils"))
                 (:file "dodolog"
			:depends-on ("packages"
				     "parameters"
				     "auth"
				     "tbnl-extensions"
				     "templates"
				     "utils"
				     "validation"))
		 (:file "i18n"
			:depends-on ("packages"))
		 (:file "packages")
		 (:file "parameters"
			:depends-on ("packages")) 
		 (:file "tbnl-extensions")
		 (:file "templates"
			:depends-on ("packages"
				     "parameters"
				     "utils"
				     "auth"
				     "db"
				     "i18n"))
		 (:file "utils"
			:depends-on ("packages"
				     "parameters"))
		 (:file "validation"
			:depends-on ("packages"
				     "parameters"
				     "utils"
				     "db"))))
