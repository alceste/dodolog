;;;; dodolog, aka yet another Common LISP blog
;;;;
;;;; Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
;;;;
;;;; This program is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation; either version 2 of
;;;; the License, or (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public
;;;; License along with this program; if not, write to the Free
;;;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;;;; MA 02111-1307, USA.

(in-package #:dodolog)

;;; DB access policy
;;;
;;; The design requirements are:
;;;
;;;     1. keep a small number of pooled read-only connections to
;;;        the DB;
;;;
;;;     2. let the DBMS do what it has been created for (sorting,
;;;        views, statistics, etc.).  Don't reinvent the wheel
;;;        Lisp-side;
;;;
;;;     3. open a dedicated connection for each write access;
;;;
;;;     4. keep transferred data at a minimum;
;;;
;;;     5. minimize code and raw SQL statements by using CLSQL view
;;;        classes.
;;;
;;; (1), (2) and (4) are for performance, (3) is needed to avoid
;;; hanging the whole blog if something goes wrong during a write
;;; access (e.g. stale transactions and/or locked tables): everything
;;; will be rolled back and unlocked when the connection is closed.
;;; (5) is for keeping the code clean.
;;;
;;; In order to make CLSQL cooperate with these goals, here's the DB
;;; layer rationale:
;;;
;;;     1. don't use CLSQL OODML out of this file (db.lisp);
;;;
;;;     2. create view classes that directly represent SQL rows, and
;;;        put them as leaf nodes in the OO inheritance hierarchy;
;;;
;;;     3. let these classes have a "db-persist" method that will take
;;;        care of serialization;
;;;
;;;     4. create SQL views with all the additional data and
;;;        properties you need for the application (statistics,
;;;        sorting, etc.) and map them to read-only view classes on
;;;        the OO hierarchy.
;;;
;;; View classes based on SQL views won't support the db-persist
;;; method, and thus won't be serializable.  The db-persist method, on
;;; the other hand, will take care of the gory details (e.g. the
;;; quirks required to overcome CLSQL limitations).

(clsql:locally-enable-sql-reader-syntax)

;;; Disable caching, even for join slots.  FIXME: a bit too strong?
(setf clsql:*default-caching* nil)

;;; Blog user
(clsql:def-view-class user ()
  ((nick :column nick
	 :type (clsql:varchar 255)
	 :initarg :nick
	 :accessor nick)
   (fullname :column fullname
	     :type (clsql:varchar 255)
	     :initarg :fullname
	     :accessor fullname)
   (email :column email
	  :type (clsql:varchar 255)
	  :initarg :email
	  :accessor email)
   (password :column password
	     :type (clsql:varchar 255)
	     :initarg :password
	     :accessor password))
  (:base-table dodolog_user))

;; Post category, with the bare name
(clsql:def-view-class abstract-category-name ()
  ((name :column name
	 :type (clsql:varchar 255)
	 :initarg :name
	 :accessor name)))

;; Post category, with the bare name
(clsql:def-view-class category-name (abstract-category-name)
  ()
  (:base-table dodolog_category))

;; Post category, with the bare name, associated through a post id
(clsql:def-view-class post-category-name (abstract-category-name)
  ((post-id :column post_id
	    :type (clsql:varchar 255)
	    :initarg :post-id
	    :accessor post-id)
   (post :db-kind :join
	 :db-info (:join-class ordered-post
		   :home-key post-id
		   :foreign-key id
		   :set nil)))
  (:base-table dodolog_ordered_post_categories_view))

;; Post category, with all the available data
(clsql:def-view-class category (category-name)
   ((description :column description
		 :type (clsql:varchar 16777216) ; 16 MiB should be enough :-)
		 :initarg :description
		 :initform ""
		 :accessor description)
    (posts :db-kind :join
	   :db-info (:join-class post-category
		     :home-key name
		     :foreign-key category-name
		     :target-slot post
		     :set t)
	   :accessor posts))
   (:base-table dodolog_category))

;; Post category, with ordered retrieval from DB
(clsql:def-view-class ordered-category (category)
   ()
   (:base-table dodolog_ordered_categories_view))

;; Post category, with ordered retrieval from DB
(clsql:def-view-class category-with-posts-count (category)
  ((posts-count :column count
		:type integer
		:accessor posts-count))
  (:base-table dodolog_ordered_categories_with_posts_count_view))

;;; Association between posts and categories
(clsql:def-view-class post-category ()
  ((post-id :column post_id
	    :type (clsql:varchar 255)
	    :initarg :post-id
	    :accessor post-id)
   (category-name :column category_name
		  :type (clsql:varchar 255)
		  :initarg :category-name
		  :accessor category-name)
   (post :db-kind :join
	 :db-info (:join-class post
		   :home-key post-id
		   :foreign-key id
		   :set nil
		   :retrieval :immediate)
	 :accessor post)
   (category :db-kind :join
	     :db-info (:join-class ordered
		       :home-key category-name
		       :foreign-key name
		       :set nil
		       :retrieval :immediate)
	     :accessor category))
  (:base-table dodolog_post_category))

;;; Post headers: no body, no join classes
(clsql:def-view-class post-headers ()
  ((id :db-kind :key
       :column id
       :type (clsql:varchar 255)
       :initform ""
       :initarg :id
       :accessor id)
   (timestamp :column timestamp
	      :type clsql:wall-time
	      :initarg :timestamp
	      :initform (clsql:get-time)
	      :accessor timestamp)
   (title :column title
	  :type (clsql:varchar 1000)
	  :initarg :title
	  :initform ""
	  :accessor title)
   (user-nick :column user_nick
	      :type (clsql:varchar 255)
	      :initarg :user-nick
	      :initform ""
	      :accessor user-nick))
  (:base-table dodolog_ordered_posts_view))

(defgeneric year-month-day (post-headers)
  (:documentation "Return year, month and day for the given post"))

(defmethod year-month-day ((post post-headers))
  (multiple-value-bind
	(usec sec minute hour date month year)
      (clsql:decode-time (timestamp post))
    (values year month date)))

;;; An abstract post on the blog, with all the related data
(clsql:def-view-class abstract-post (post-headers)
  ((body :column body
	 :type (clsql:varchar 16777216) ; 16 MiB should be enough :-)
	 :initarg :body
	 :initform ""
	 :accessor body)
   (category-names :db-kind :join
		   :db-info (:join-class post-category-name
			     :home-key id
			     :foreign-key post-id
			     :set t)
	       :accessor category-names)
   (comments :db-kind :join
	     :db-info (:join-class comment-with-depth
		       :home-key id
		       :foreign-key post-id
		       :set t)
	     :accessor comments)
   (user :db-kind :join
	 :db-info (:join-class user
		   :home-key user-nick
		   :foreign-key nick
		   :set nil)
	 :accessor user)))

;;; A post on the blog
(clsql:def-view-class post (abstract-post)
  ()		      
  (:base-table dodolog_post))

;;; A post on the blog, with all the related data and a comments count
(clsql:def-view-class ordered-post-with-comments-count (abstract-post)
  ((comments-count :column comments_count
		   :type integer
		   :accessor comments-count))
  (:base-table dodolog_ordered_posts_with_comments_count_view))

;;; A post on the blog, with ordered retrieval based on date
(clsql:def-view-class ordered-post (abstract-post)
  ()
  (:base-table dodolog_ordered_posts_view))

;;; A post on the blog, with ordered retrieval based on category
(clsql:def-view-class category-ordered-post-with-comments-count (ordered-post-with-comments-count)
  ((category-name :column category_name
		  :type (clsql:varchar 255)
		  :accessor category-name))
  (:base-table dodolog_ordered_category_archive_view))

;;; A post on the blog, with ordered retrieval based on year and month
(clsql:def-view-class date-ordered-post-with-comments-count (ordered-post-with-comments-count)
  ((year :column year
	 :type integer
	 :accessor year)
   (month :column month
	  :type integer
	  :accessor month))
  (:base-table dodolog_ordered_date_archive_view))

;;; Post comments
(clsql:def-view-class abstract-comment ()
  ((id :db-kind :key
       :column id
       :type integer
       :initarg :id
       :accessor id)
   (post-id :column post_id
	    :type (clsql:varchar 255)
	    :initarg :post-id
	    :accessor post-id)
   (timestamp :column timestamp
	      :type clsql:wall-time
	      :initarg :timestamp
	      :initform (clsql:get-time)
	      :accessor timestamp)
   (user-nick :column user_nick
	      :type (clsql:varchar 255)
	      :initarg :user-nick
	      :accessor user-nick)
   (author :column author
	   :type (clsql:varchar 255)
	   :initarg :author
	   :accessor author)
   (email :column email
	  :type (clsql:varchar 50)
	  :initarg :email
	  :accessor email)
   (url :column url
	:type (clsql:varchar 255)
	:initarg :url
	:accessor url)
   (in-reply-to :column in_reply_to
		:type integer
		:initarg :in-reply-to
		:accessor in-reply-to)
   (title :column title
	  :type (clsql:varchar 255)
	  :initarg :title
	  :accessor title)
   (body :column body
	 :type (clsql:varchar 8192)
	 :initarg :body
	 :accessor body)
   (ip-address :column ip_address
	       :type (clsql:varchar 15)
	       :initarg :ip-address
	       :accessor ip-address)
   (deleted-p  :column isdeleted
	       :type boolean
	       :initarg :deleted-p
	       :accessor deleted-p)
   (user :db-kind :join
	 :db-info (:join-class user
		   :home-key user-nick
		   :foreign-key nick
		   :set nil)
	 :accessor user)
   (parent :db-kind :join
	   :db-info (:join-class comment
		     :home-key in-reply-to
		     :foreign-key id
		     :set nil)
	   :accessor parent)
   (replies :db-kind :join
	    :db-info (:join-class comment
		      :home-key id
		      :foreign-key in-reply-to
		      :set t)
	    :accessor replies)
   (post :db-kind :join
	 :db-info (:join-class post
		   :home-key post-id
		   :foreign-key id
		   :set nil)
	 :accessor post)))

;;; Post comment
(clsql:def-view-class comment (abstract-comment)
  ()
  (:base-table dodolog_comment))

;;; Post comments with depth, for building a tree of replies
(clsql:def-view-class comment-with-depth (abstract-comment)
  ((depth :column depth
	  :type integer
	  :accessor depth))
  (:base-table dodolog_comments_tree_view))

;;; Years and months for date archive
(clsql:def-view-class archive-date ()
  ((year :column year
	 :type integer
	 :accessor year)
   (month :column month
	  :type integer
	  :accessor month))
  (:base-table dodolog_archive_dates_view))

;;; Years and months for date archive, with posts count
(clsql:def-view-class archive-date-with-posts-count (archive-date)
  ((posts-count :column count
		:type integer
		:accessor posts-count))
  (:base-table dodolog_archive_dates_with_posts_count_view))

;;; Common stuff
(defun -db-ro-connection ()
  "Get a read-only connection to the DB, possibly reusing existing ones"
  (clsql:connect *dodolog-db-connection-parameters*
		 :database-type *dodolog-db-type* :pool t))

(defmacro -db-select (&rest what)
  "Perform a select with default parameters for caching, etc."
  `(clsql:select ,@what
		 :flatp t
		 :result-types :auto
		 :field-names nil
		 :database (-db-ro-connection)))

(defmacro -db-select-object (object &rest args)
  "Perform a select for an object with default parameters for caching, etc."
  `(clsql:select ,object
		 ,@args
		 :flatp t
		 :refresh t
		 :caching nil
		 :database (-db-ro-connection)))

(defmacro -db-query (query &rest args)
  "Perform a query with default parameters for caching, DB connection, etc."
  `(clsql:query ,query
		,@args
		:result-types :auto
		:flatp t
		:field-names nil
		:database (-db-ro-connection)))

(defgeneric db-persist (object &rest args)
  (:documentation "Persist the given object on the DB"))

(defmethod db-persist ((post post) &rest args)
  (clsql:with-database (rw-connection *dodolog-db-connection-parameters*
				      :database-type *dodolog-db-type*
				      :if-exists :new)
    (clsql:with-transaction (:database rw-connection)
      (let ((original-id (getf args :original-id))
	    (selected-category-names (getf args :selected-category-names))
	    (new-category-names (getf args :new-category-names)))
	(dolist (c new-category-names)
	  (clsql:update-records-from-instance c :database rw-connection))
	;; Remove old post-category associations (if any)
	;; No OODML, since the change is trivial (and much faster this way)
	(clsql:delete-records :from [dodolog_post_category]
			      :where [= [post_id] original-id]
			      :database rw-connection)
	;; FIXME: ugly kludge due to CLSQL lack of support for updating pkeys
	(clsql:update-records [dodolog_post]
			      :av-pairs`((id ,(id post)))
			      :where [= [id] original-id])
	(clsql:update-records-from-instance post :database rw-connection)
	;; (Re)assign post to categories
	(dolist (c (concatenate 'list
				selected-category-names
				new-category-names))
	  (let ((post-category (make-instance 'post-category
					      :post-id (id post)
					      :category-name (name c))))
	    (clsql:update-records-from-instance post-category
						:database rw-connection))))))
  ;; Rebind DB connection to the one with read-only access
  (setf (slot-value post 'clsql-sys::view-database) (-db-ro-connection)))

(defgeneric db-delete (object &rest args)
  (:documentation "Delete the given object from the DB"))

(defmethod db-delete ((post post) &rest args-unused)
  (clsql:with-database (rw-connection *dodolog-db-connection-parameters*
				      :database-type *dodolog-db-type*
				      :if-exists :new)
    ;; Rebind DB connection to the new one with read-write access
    (setf (slot-value post 'clsql-sys::view-database) rw-connection)
    (clsql:delete-instance-records post)))

(defun db-get-posts-with-comments-count (&key (limit 10))
  "Return the last posts"
  (-db-select-object 'ordered-post-with-comments-count
		     :limit limit))

(defun db-get-post (id)
  "Return the post with the specified id from the DB"
  (first (-db-select-object 'post
			    :where [= [id] id])))

(defun db-get-post-with-comments-count (id)
  "Return the post (with comments count) with the specified id from the DB"
  (first (-db-select-object 'ordered-post-with-comments-count
			    :where [= [id] id])))

(defun db-post-id-exists-p (id)
  "Returns t if the specified post ID already exists on the DB"
  (not
   (eq 0
       (length
	;; FIXME: try to use CLSQL symbolic syntax
	(-db-select-object 'post
			    :where [= [id] (string-downcase (string-trim
							     +form-whitespaces+
							     id))])))))

(defun db-get-posts-headers (&optional &key (limit 10))
  "Return the last posts from the DB"
  (-db-select-object 'post-headers
		     :limit limit))

(defun db-get-category-with-posts-count (name)
  "Return the posts on the DB with the given category, and their count"
  (first (-db-select-object 'category-with-posts-count
			    :where [= [name] name])))

(defun db-get-category-archive (category &key (start 0) (limit 10))
  "Return the archive for the given category"
  (-db-select-object 'category-ordered-post-with-comments-count
		     :where [= [category-name] (name category)]
		     :offset start
		     :limit limit))

(defun db-get-archive-dates ()
  "Return years and months for date archives"
  (-db-select-object 'archive-date))

(defun db-get-archive-dates-with-posts-count ()
  "Return years and months for date archives, with posts count"
  (-db-select-object 'archive-date-with-posts-count))

(defun db-get-archive-date-with-posts-count (year month)
  "Return the archive-date-with-posts-count object for the given date"
  (first (-db-select-object 'archive-date-with-posts-count
			    :where [and [= [year] year]
			                [= [month] month]])))

(defun db-get-date-archive (year month
			    &key (start 0) (limit 10))
  "Return posts for the date archive"
  (-db-select-object 'date-ordered-post-with-comments-count
		     :where [and [= [year] year]
                                 [= [month] month]]
		     :offset start
		     :limit limit))


(defun db-get-comment (&key post id)
  "Get the specified comment for the specified post"
  (first (-db-select-object 'comment
			    :where [and [= [post-id] (id post)]
                                        [= [id] id]])))

(defun db-comment-has-replies-p (comment)
  "Return t if a comment has replies"
  (< 0
     (length
      (-db-select-object 'comment 
			 :where [and [= [post-id] (id (post comment))]
			             [= [in-reply-to] (id comment)]]))))

(defmethod db-delete ((comment comment) &rest args)
  "Delete the given comment object"
  (clsql:with-database (rw-connection *dodolog-db-connection-parameters*
				      :database-type *dodolog-db-type*
				      :if-exists :new)
    (clsql:with-transaction (:database rw-connection)
      (let* ((replies-action (getf args  :replies-action))
	     (comment-id (id comment))
	     (parent (parent comment))
	     (parent-id (when parent
			  (id parent)))
	     (post-id (id (post comment))))
	(when (eq replies-action :reparent-replies)
	  ;; No OODML, since the change is trivial (and much faster this way)
	  (clsql:update-records [dodolog-comment]
				:av-pairs `(([in-reply-to] ,parent-id))
				:where [and [= [post-id] post-id]
				            [= [in-reply-to] comment-id]]
				:database rw-connection))
	(if (eq replies-action :empty-comment)
	    (progn
	      (setf (deleted-p comment) t) ; FIXME: keep the comment content?
	      (clsql:update-records-from-instance comment
						  :database rw-connection))
	  ;; No OODML, since the change is trivial (and much faster this way)
	  (clsql:delete-records :from [dodolog-comment]			  
				:where [and [= [post-id] post-id]
				               [= [id] comment-id]]
				:database rw-connection))))))

(defmethod db-persist ((comment comment) &rest args-unused)
  "Publish the specified comment.  Return the ID of the new comment"
  (clsql:with-database (rw-connection *dodolog-db-connection-parameters*
				      :database-type *dodolog-db-type*
				      :if-exists :new)
    (clsql:with-transaction (:database rw-connection)
      (clsql:update-records-from-instance comment :database rw-connection)
      ;; FIXME: it's definitely PostgreSQL-specific
      (setf (id comment)
	    (clsql:sequence-last "dodolog_comment_id_seq"
				 :database rw-connection))))
  ;; Rebind DB connection to the one with read-only access
  (setf (slot-value comment 'clsql-sys::view-database) (-db-ro-connection)))
				       
(defun db-get-categories ()
  "Get all the categories in the DB"
  (-db-select-object 'category
		     :order-by '([name])))

(defun db-get-category-names ()
  "Return the category names from the DB"
  (-db-select-object 'category-name
		     :order-by '([name])))

(defun db-get-user-with-password (user-nick password)
  "Get the user with the given nick and password, or nil if it doesn't exist"
  (first (-db-select-object 'user
			    :where [and [= [nick] user-nick]
			                [= [password] password]])))

(clsql:restore-sql-reader-syntax-state)
