;;;; dodolog, aka yet another Common LISP blog
;;;;
;;;; Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
;;;;
;;;; This program is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation; either version 2 of
;;;; the License, or (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public
;;;; License along with this program; if not, write to the Free
;;;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;;;; MA 02111-1307, USA.

(in-package #:dodolog)

(defconstant +form-whitespaces+
  (make-array 3 :element-type 'character
	      :initial-contents '(#\Space #\Return #\Newline))
  "Characters used to represent newlines in web forms")

(defun build-regex-dispatchers (dispatchers-list)
  "Build a list of regex dispatchers, to be used with TBNL dispatch table"
  (mapcar #'(lambda (arg)
	      (apply #'create-regex-dispatcher
		     (list (make-regex-with-path-prefix (first arg))
			   (second arg))))
	  dispatchers-list))

(defmacro build-groups-bind-regex-dispatchers (dispatchers-list)
  "Build a list of groups-bind regex dispatchers, to be used with TBNL"
  `(list
    ,@(let ((rlist (gensym)))
	   (setf rlist nil)
	   (dolist (d dispatchers-list)
	     (setf rlist
		   (nconc rlist
			  `((tbnl::create-groups-bind-regex-dispatcher ,@d)))))
	   rlist)))

(defmacro with-post ((post year month day post-id) &body body)
  "Execute some code with the specified post variable bound to
the post object with the given ID.  Redirect to the 404 page if
post-id doesn't exist on the DB"
  `(let ((,post (db-get-post ,post-id)))
     (if (not ,post)
	 (404-page :message "Post not found")
	 (progn
	   (when (and ,year ,month ,day)
	       (multiple-value-bind (pyear pmonth pday)
		   (year-month-day post)
		 (when (not (and (= ,year pyear)
				 (= ,month pmonth)
				 (= ,day pday)))
		 (404-page :message "Post not found"))))
	   ,@body))))

(defmacro with-post-with-comments-count ((post year month day post-id)
					 &body body)
  "Execute some code with the specified post variable bound to
the post object (with comments count) with the given ID.
Redirect to the 404 page if post-id doesn't exist on the DB"
  ;; FIXME: duplication with above function...
  `(let ((,post (db-get-post-with-comments-count ,post-id)))
     (if (not ,post)
	 (404-page :message "Post not found")
	 (progn
	   (when (and ,year ,month ,day)
	       (multiple-value-bind (pyear pmonth pday)
		   (year-month-day post)
		 (when (not (and (= ,year pyear)
				 (= ,month pmonth)
				 (= ,day pday)))
		 (404-page :message "Post not found"))))
	   ,@body))))

(defmacro with-comment ((comment (post comment-id)) &body body)
  "Execute some code with the specified comment variable bound to
the comment object identified by the given post and comment IDs.
Redirect to the 404 page if the comment doesn't exist on the DB"
  `(let ((,comment (db-get-comment :post ,post :id ,comment-id)))
     (if (not ,comment)
	 (404-page :message (format nil
				    "Comment &quot;~D&quot; not found"
				    ,comment-id))
	 (progn ,@body))))

(defun make-regex-with-path-prefix (regex)
  "Concatenate dodolog path prefix with the supplied regex"
  (concatenate 'string
	       (kmrcl:substitute-chars-strings *dodolog-http-path-prefix*
					       '((#\/ . "\\/")))
	       regex))

(defun empty-when-trimmed-p (string)
  "Return t if the given argument is an empty string when trimmed, or nil"
  (if (null string)
      t
      (zerop (length (string-trim +form-whitespaces+ string)))))

(defun nil-if-empty (string)
  "Return the string if it's not empty, or nil"
  (if (zerop (length string))
      nil
      string))
