;;;; dodolog, aka yet another Common LISP blog
;;;;
;;;; Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
;;;;
;;;; This program is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation; either version 2 of
;;;; the License, or (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public
;;;; License along with this program; if not, write to the Free
;;;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;;;; MA 02111-1307, USA.

(in-package #:dodolog)

;;; Blog parameters
(defparameter *dodolog-blog-title* "My Cool Blog"
  "Title of the blog")
(defparameter *dodolog-blog-description* "John's blog"
  "Description of the blog")
(defparameter *dodolog-blog-author* "John Random Hacker"
  "Main author of the blog")
(defparameter *dodolog-blog-keywords* "lisp software pr0n"
  "Keywords for the main page")

;;; Web server details
(defparameter *dodolog-http-port* 3000
  "Port on which dodolog will listen for connections from mod_lisp or the web")
(defparameter *dodolog-http-host* "blog.domain.tld"
  "Hostname of the server running dodolog (will be used to build URLs)")
(defparameter *dodolog-http-path-prefix* "/blog"
  "Path following the host in dodolog URLs. Don't put a trailing slash!")

;;; SQL DBMS connection
(defparameter *dodolog-db-type* :postgresql-socket
  "Type of DBMS to connect to (must be supported by CLSQL)")
(defparameter *dodolog-db-connection-parameters*
  '("127.0.0.1" "dodolog-db" "dodolog-user" "password")
  "Parameters for connecting to the DB.  DBMS-specific: see CLSQL docs")

;;; Email notifications
(defparameter *dodolog-mail-host* "mail.domain.tld"
  "SMTP host to be used for sending emails")
(defparameter *dodolog-mail-from-field* "Dodolog <dodolog@domain.tld>"
  "Value of the 'From:' field outgoing in emails")
(defparameter *dodolog-mail-subject-prefix* "[Dodolog] "
  "Prefix for the subject of outgoing emails")
