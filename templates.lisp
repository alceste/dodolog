;;;; dodolog, aka yet another Common LISP blog
;;;;
;;;; Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
;;;;
;;;; This program is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation; either version 2 of
;;;; the License, or (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public
;;;; License along with this program; if not, write to the Free
;;;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;;;; MA 02111-1307, USA.

(in-package #:dodolog)

;;; Some settings for HTML-TEMPLATE
(setf html-template:*template-start-marker* "<!--{")
(setf html-template:*template-end-marker* "}-->")
(setf html-template:*default-template-pathname*
      (merge-pathnames (make-pathname :directory '(:relative "templates"))
		       *dodolog-package-pathname*))

(defun tmpl-show-404-page (&key message)
  "Return the main page template filled with the proper data"
  (-process-template "main-page" (list :dodolog-include-login-link t
				       :dodolog-include-error t
				       :dodolog-error-message message)))

(defun tmpl-show-main-page ()
  "Return the main page template filled with the proper data"
  (let ((posts (-build-posts-plist (db-get-posts-with-comments-count)
				   :with-comments-count-p t)))
    (-process-template "main-page" (list :dodolog-include-sidebar t
					 :dodolog-include-posts t
					 :dodolog-include-login-link t
					 :posts posts
					 :dodolog-last-posts posts))))

(defun tmpl-show-single-post-page (post
				   &key (errors nil)
				   (comment nil)
				   (comment-author "")
				   (comment-email "")
				   (comment-url "")
				   (comment-title "")
				   (comment-body ""))
  "Return a single-post page template filled with the proper data"
  (-process-template "main-page"
		     (list :dodolog-include-sidebar t
			   :dodolog-include-posts t
			   :dodolog-include-login-link t
			   :dodolog-include-comments t
			   :posts (list
				   (nconc
				    (-build-post-plist
				     post
				     :with-comments-p t
				     :with-comments-count-p t
				     :show-comments-count-in-footer-p nil)
				    errors
				    (list :comment-author (escape-for-html
							   comment-author)
					  :comment-email (escape-for-html
							  comment-email)
					  :comment-url (escape-for-html
							 comment-url)
					  :comment-title (escape-for-html
							  comment-title)
					  :comment-body (escape-for-html
							 comment-body))))
			   :dodolog-last-posts (-build-posts-headers-plist
						(db-get-posts-headers)))))
  
(defun tmpl-show-login-page (&key (request nil) (errors nil)
			     (dodolog-error nil) (came-from nil))
  "Return the login page template filled with the proper data"
  (-process-template "main-page"
		     (nconc (list :dodolog-include-login t
				  :dodolog-error dodolog-error)
			    (when came-from
			      (list :came-from (escape-for-html came-from)))
			    (when request
			      (list :user-nick (post-parameter "user-nick"
							       request)))
			    errors)))

(defun tmpl-show-logout-page ()
  "Return the logout page template filled with the proper data"
  (-process-template "main-page" (list :dodolog-include-logout t
				       :dodolog-include-login-link t)))

(defun tmpl-show-edit-post-page (post &key (errors nil)
				      (selected-category-names nil)
				      (new-categories-raw ""))
  "Return the post editing page filled with the proper data"
  (-process-template "main-page"
		     (nconc
		      (list :dodolog-include-sidebar t
			    :dodolog-include-edit-post t
			    :dodolog-last-posts (-build-posts-headers-plist
						 (db-get-posts-headers))
			    :categories
			     (-build-category-names-plist
			      (db-get-category-names)
			      :selected-category-names
			       selected-category-names)
			     :new-categories-raw (escape-for-html
						  new-categories-raw))
		      errors
		      (-build-post-plist post))))

(defun tmpl-show-post-preview-page (post &key
					 selected-category-names
					 new-category-names new-categories-raw)
  "Return the post preview page filled with the proper data"
  (-process-template "main-page"
		     (list :dodolog-include-sidebar t
			   :dodolog-include-posts t
			   :dodolog-include-post-preview t
			   :dodolog-last-posts (-build-posts-headers-plist
						(db-get-posts-headers))
			   :posts (list (nconc
					 (-build-post-plist
					  post
					  :show-user-menu-p nil
					  :with-comments-count-p t
					  :force-comments-count 0)
					 (list
					  :selected-category-names
					  (-build-category-names-plist
					   selected-category-names)
					  :new-category-names
					  (-build-category-names-plist
					   new-category-names)
					  :new-categories-raw
					  new-categories-raw))))))

(defun tmpl-show-post-delete-page (&key post-id post-title
				   (post-not-yet-deleted t))
  "Return the post deletion template filled with the proper data"
  (-process-template "main-page"
		     (list :dodolog-include-delete-post t
			   :post-id (escape-for-html post-id)
			   :post-title (escape-for-html post-title)
			   :post-not-yet-deleted post-not-yet-deleted)))

(defun tmpl-show-comment-page (post
			       &key comment (errors nil)
			       (comment-author "")
			       (comment-email "")
			       (comment-url "")
			       (comment-title "")
			       (comment-body ""))
  "Return the comment template filled with the proper data"
  (-process-template "main-page"
		     (nconc
		      errors
		      (list :dodolog-include-sidebar t
			    :dodolog-include-comment-reply t
			    :dodolog-include-login-link t
			    :dodolog-commented-post-title-html
			    (escape-for-html (title post))
			    :dodolog-commented-post-id-url
			    (url-encode (id post))
			    :dodolog-last-posts (-build-posts-headers-plist
						 (db-get-posts-headers))
			    :reply-to-comment-id (format nil "~D" (id post))
			    :comments (-build-comments-plist
				       (list comment) :with-depth-p nil
				       :show-reply-to-link-p nil)
			    :comment-author (escape-for-html
					     comment-author)
			    :comment-email (escape-for-html comment-email)
			    :comment-url (escape-for-html comment-url)
			    :comment-title (escape-for-html comment-title)
			    :comment-body (escape-for-html comment-body)))))

(defun tmpl-show-comment-delete-page (comment
				      &key (comment-not-yet-deleted-p t))
  "Return the comment deletion template filled with the proper data"
  (let ((post (post comment)))
    (multiple-value-bind (year month day)
	(year-month-day post)
      (-process-template
       "main-page"
       (list
	:year (format nil "~D" year)
	:month (format nil "~D" month)
	:date (format nil "~D" day)
	:dodolog-include-delete-comment t
	:comment-id-html (escape-for-html
			  (format nil "~D" (id comment)))
	:comment-title-html (escape-for-html (or (title comment)
						 "(No title)"))
	:post-id-url (url-encode (id post))
	:post-title-html (escape-for-html (title post))
	:comment-has-replies-p (and comment-not-yet-deleted-p
				    (db-comment-has-replies-p
				     comment))
	:comment-not-yet-deleted-p comment-not-yet-deleted-p)))))
  
(defun tmpl-show-category-archive (category-w-posts-count
				   &key (start 0) (batch-size 10))
  "Return the category archive page filled with the proper data"
  (-show-archive :posts (db-get-category-archive category-w-posts-count
						 :start start
						 :limit batch-size)
		 :count (posts-count category-w-posts-count)
		 :archive-title (format nil
					"Archive for category &quot;~A&quot;"
					(name category-w-posts-count))
		 :start start :batch-size batch-size))

(defun tmpl-show-date-archive (year month
			       &key (start 0) (batch-size 10))
  "Return the category archive page filled with the proper data"
  (-show-archive :posts (db-get-date-archive year month
					     :start start :limit batch-size)
		 :count (posts-count (db-get-archive-date-with-posts-count
				      year month))
		 :archive-title (format nil "Archive for ~A ~D"
					(nth month *i18n-month-names*)
					year)
		 :start start :batch-size batch-size))

(defun -show-archive (&key posts count archive-title (start 0) (batch-size 10))
  "Return a generic archive page filled with the given parameters.
posts-count-fun must return two values: posts in this batch, and total count"
  (-process-template
   "main-page"
   (nconc (-build-page-batches-plist count batch-size start)
	  (list :dodolog-include-sidebar t
		:dodolog-include-posts t
		:dodolog-include-login-link t
		:dodolog-archive-title archive-title
		:dodolog-last-posts (-build-posts-headers-plist
				     (db-get-posts-headers))
		:posts (-build-posts-plist posts
					   :with-comments-count-p t)))))

(defun -build-page-batches-plist (count batch-size current-start)
  "Build a plist to feed the navigation element with the given number of pages"
  (when (> count batch-size)
    (let ((prev-batch (- current-start batch-size))
	  (next-batch (+ current-start batch-size)))
      (list 
       :dodolog-prev-batch (when (>= prev-batch 0)
			     (format nil "~D" prev-batch))
       :dodolog-next-batch (when (<= next-batch count)
			     (format nil "~D" next-batch))
       :dodolog-batch-pages
        (let ((npages (ceiling (/ count batch-size))))
	  (loop for n from 0 to (1- npages)
	     collect (list :number (format nil "~D" (1+ n))
			   :not-current (not (= (* batch-size n)
						current-start))
			   :start (format nil "~D" (* batch-size n)))))))))

(defun tmpl-show-comment-notification-email (comment)
  "Return the comment notification email body filled with the proper data"
  (let ((post (post comment)))
    (-process-template "notification-email"
		       (list :dodolog-http-host *dodolog-http-host*
			     :dodolog-http-path-prefix
			      *dodolog-http-path-prefix*
			     :comment-id (format nil "~D" (id comment))
			     :comment-ip-address (ip-address comment)
			     :comment-author (or (author comment) "")
			     :comment-email (or (email comment) "")
			     :comment-url (or (url comment) "")
			     :comment-title (or (title comment) "(No title)")
			     :comment-body (body comment)
			     :post-title (title post)
			     :post-id-url (url-encode (id post))))))

(defun tmpl-get-selected-category-names (request)
  "Get the existing categories chosen by user in the post form"
  (remove-if-not #'(lambda (c) (post-parameter (concatenate 'string
							    "category:"
							    (name c))
					       request))
		 (db-get-category-names)))

(defun tmpl-get-new-category-names (request)
  "Get a list of new categories specified in the post form"
  (let ((categories nil))
    (dolist (c (split-sequence:split-sequence #\Newline
					      (post-parameter "new-categories"
							      request)))
      (let ((cat (string-trim +form-whitespaces+ c)))
	(when (not (zerop (length cat)))
	  (setf categories (nconc
			    (list (make-instance 'category-name :name cat))
			    categories)))))
    categories))

(defun -build-posts-headers-plist (posts-headers)
  "Build property lists for the given list of posts headers"
  (mapcar #'-build-post-headers-plist posts-headers))

(defun -build-post-headers-plist (post-headers)
  "Get a property list from a post headers instance"
  (let ((id (id post-headers))
	(title (title post-headers))
	(user-nick (user-nick post-headers)))
    (list :id-html         (escape-for-html id)
	  :id-url          (url-encode id) 
	  :title-html      (escape-for-html title)
	  :user-nick-html  (escape-for-html user-nick)
	  :user-nick-url   (url-encode user-nick)
	  :dodolog-http-path-prefix *dodolog-http-path-prefix*)))

(defun -build-posts-plist (posts &key (with-comments-count-p nil))
  "Build property lists for the given list of posts"
  (mapcar #'(lambda (p)
	      (-build-post-plist p
				 :with-comments-count-p with-comments-count-p))
	  posts))

(defun -build-post-plist (post
			  &key (with-comments-p nil) (show-user-menu-p t)
			  (with-comments-count-p nil)
			  (show-comments-count-in-footer-p t)
			  (force-comments-count nil))
  "Get a property list from a post instance"
  (let ((body (body post))
	(user (user post)))
    (nconc
     (-build-post-headers-plist post)
     (list :body            body
	   :body-html       (escape-for-html body)
	   :show-comments-count-in-footer-p show-comments-count-in-footer-p
	   :with-comments-count-p with-comments-count-p 
	   :comments-count  (when with-comments-count-p
			      (format nil "~D"
				      (if (not (null force-comments-count))
					  force-comments-count
					  (comments-count post))))
	   :categories      (-build-category-names-plist (category-names post))
	   ;; User menu
	   :dodolog-http-path-prefix *dodolog-http-path-prefix*
	   :show-user-menu-p (and show-user-menu-p
				  (not (auth-anonymous-user-p))
				  (not (null user))
				  (string=
				   (nick user)
				   (nick (auth-get-authenticated-user))))
	   ;; Comments
	   :with-comments-p with-comments-p
	   :comments (when with-comments-p
		       (-build-comments-plist (comments post))))
     (-build-timestamp-plist (timestamp post)))))

(defun -build-timestamp-plist (timestamp)
  "Build a plist for a CLSQL timestamp"
  (multiple-value-bind
      (usec second minute hour date month year day-of-week)
      (clsql:decode-time timestamp)
    (list
     :usec        (format nil "~D" usec)
     :second      (format nil "~2,'0D" second)
     :minute      (format nil "~2,'0D" minute)
     :hour        (format nil "~2,'0D" hour)
     :date        (format nil "~D" date)
     :month       (format nil "~D" month)
     :year        (format nil "~D" year)
     :day-of-week (format nil "~D" day-of-week)
     :day-name    (nth day-of-week *i18n-day-names*)
     :month-name  (nth month *i18n-month-names*))))

(defun -build-comments-plist (comments
			      &key (show-reply-to-link-p t) (with-depth-p t))
  "Build a plist with the given comments"
  (when comments
    (multiple-value-bind (year month day)
	(year-month-day (post (first comments)))
      (mapcar #'(lambda (c)
		  (-build-comment-plist c
					:year year :month month :day day
					:show-reply-to-link-p
					 show-reply-to-link-p
					:with-depth-p with-depth-p))
	      comments))))

(defun -build-comment-plist (c &key year month day
			     show-reply-to-link-p with-depth-p)
  "Build a plist for the given comment"
  (let* ((post (post c))
	 (post-id (post-id c))
	 (author (author c))
	 (email (email c))
	 (url (url c))
	 (title (title c))
	 (id (id c))
	 (post-user-nick (user-nick post))
	 (auth-user (auth-get-authenticated-user)))
    (nconc
     (list :id (format nil "~D" id)
	   :year (format nil "~D" year)
	   :month (format nil "~D" month)
	   :date (format nil "~D" day)
	   :post-id-html (escape-for-html post-id)
	   :post-id-url (url-encode post-id)
	   :author (if author
		       (escape-for-html author)
		       "Anonymous")
	   :email (when email
		    (escape-for-html (-mangle-email email)))
	   :url (when url (escape-for-html url))
	   :title (if title
		      (escape-for-html title)
		      "(No title)")
	     :body  (kmrcl:substitute-chars-strings
		     (escape-for-html (body c))
		     '((#\Newline . "<br />")))
	     :deleted-p (deleted-p c)
	     :depth (when with-depth-p
		      (format nil "~D" (min 50 (* 5 (or (depth c) 0)))))
	     :dodolog-http-path-prefix *dodolog-http-path-prefix*
	     :show-reply-to-link-p show-reply-to-link-p
	     :show-user-menu (and
			      (not (null auth-user))
			      (string-equal
			       (nick auth-user)
			       post-user-nick)))
     (-build-timestamp-plist (timestamp c)))))

(defun -build-category-names-plist (category-names
				    &key (selected-category-names nil))
  "Build plists for all the categories in the given list.  Mark as :selected-p
the ones present in the selected-category-names"
  (let ((selected-names (mapcar #'name selected-category-names)))
    (mapcar #'(lambda (c) (nconc (-build-category-name-plist c)
				 (list :selected-p (member (name c)
							   selected-names
							   :test #'string=))))
	    category-names)))

(defun -build-category-name-plist (cname)
  "Build a plist for the given category"
  (let ((name (name cname)))
    (list :name name
	  :name-url (url-encode name)
	  :name-html (escape-for-html name)
	  :dodolog-http-path-prefix *dodolog-http-path-prefix*)))

(defun -build-archive-dates-plist ()
  "Build a plist of years and months of the posts archive"
  (mapcar #'(lambda (arch-date)
	      (let ((y (year arch-date))
		    (m (month arch-date)))
		(list :dodolog-http-path-prefix *dodolog-http-path-prefix*
		      :year (format nil "~D" y)
		      :month (format nil "~D" m)
		      :month-name (nth m *i18n-month-names*))))
	  (db-get-archive-dates)))

(defun -build-authentication-plist ()
  "Build a plist with authenticated user data, or nil if user is anonymous"
  (let ((user (auth-get-authenticated-user)))
    (if user
	(list :dodolog-auth-user-nick     (nick user)
	      :dodolog-auth-user-fullname (fullname user)
	      :dodolog-auth-user-email    (email user))
	nil)))

(defun -template-pathname (name)
  "Return the complete pathname for the template with the given name"
  (make-pathname  :name name :type "tpl" :version nil))

(defun -process-template (template data)
  "Return a string computed by processing the specified template
with the given fill data. The fill data will be integrated with
dodolog default variables (dodolog-title, dodolog-subtitle, etc.)"
  (let ((dodolog-vars (list
		       :dodolog-http-path-prefix *dodolog-http-path-prefix*
		       :dodolog-blog-title-html (escape-for-html
						 *dodolog-blog-title*)
		       :dodolog-blog-keywords-html (escape-for-html
						    *dodolog-blog-keywords*)
		       :dodolog-blog-description-html
		        (escape-for-html *dodolog-blog-description*)
		       :dodolog-blog-author-html (escape-for-html
						  *dodolog-blog-author*)
		       :dodolog-archive-dates (-build-archive-dates-plist)))
	(auth-vars (-build-authentication-plist)))
    (with-output-to-string (html-template:*default-template-output*)
      (html-template:fill-and-print-template (-template-pathname template)
					     (nconc auth-vars
						    dodolog-vars
						    data)))))

(defun -mangle-email (email)
  "Mangle an email address, just to make spammers' life a bit harder"
  (kmrcl:substitute-chars-strings email
				  '((#\@ . " (at) ") (#\. . " (dot) "))))
