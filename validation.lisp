;;;; dodolog, aka yet another Common LISP blog
;;;;
;;;; Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
;;;;
;;;; This program is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation; either version 2 of
;;;; the License, or (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public
;;;; License along with this program; if not, write to the Free
;;;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;;;; MA 02111-1307, USA.

(in-package #:dodolog)

(defmacro -add-error (errors error-id error-message)
  "Add an entry to the accumulated validation errors"
  `(setf ,errors (nconc (list ,error-id ,error-message) ,errors)))

(defun validate-post (post selected-category-names new-category-names
		      &key original-id)
  "Validate a new post"
  (let ((errors nil)
	(id (id post))
	(title (title post))
	(body (body post)))
    (if (empty-when-trimmed-p id)
	(-add-error errors :errors-id "Please specify an ID for the post")
      (when (and (not (when original-id (string-equal (id post)
						      original-id)))
		   (db-post-id-exists-p (id post)))
	(-add-error errors :errors-id (concatenate 'string "The ID &quot;"
						   (escape-for-html id)
						   "&quot; already exists"))))
    (when (empty-when-trimmed-p title)
      (-add-error errors :errors-title "Please specify a title for the post"))
    (when (empty-when-trimmed-p body)
      (-add-error errors :errors-body "Please insert the body of the post"))
    (when (and (null selected-category-names) (null new-category-names))
      (-add-error errors
		  :errors-categories "Please specify at least one category"))
    ;; FIXME: add check for uniqueness of new categories
    errors))

(defun validate-login (user-nick password)
  "Validate login parameters"
  (let ((errors nil))
    (when (empty-when-trimmed-p user-nick)
      (-add-error errors :errors-user-nick
		 "Please insert your nick (aka username)"))
    (when (empty-when-trimmed-p password)
      (-add-error errors :errors-password "Please insert your password"))
    errors))

(defun validate-comment (author email url title body)
  "Validate a comment"
  (let ((errors nil))
    (if (null body)
	(-add-error errors :errors-comment-body
		    "Please insert the body of your comment")
	(when (> (length body) 8192)
	  (-add-error errors :errors-comment-body
		      "Comment body too long")))
    errors))