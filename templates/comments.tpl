<!-- Start comments -->

<div id="comments">
  <!--{ TMPL_IF comments }-->

    <!--{ TMPL_IF dodolog-commented-post-id-url }-->
      <h3>Comment for post
	&quot;<a class="link-in-title"
	   href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/archive/<!--{ TMPL_VAR year }-->/<!--{ TMPL_VAR month }-->/<!--{ TMPL_VAR date }-->/<!--{ TMPL_VAR dodolog-commented-post-id-url }-->"
	   title="Go back to post &quot;<!--{ TMPL_VAR dodolog-commented-post-title-html }-->&quot;"><!--{ TMPL_VAR dodolog-commented-post-title-html }--></a>&quot;
	   </h3>
    <!--{ TMPL_ELSE }-->
      <h3>
	<!--{ TMPL_VAR comments-count }--> comments
	- <a class="link-in-title"
	     href="#comment-form"
	     title="Jump to comment submission">add yours</a>
      </h3>
    <!--{ /TMPL_IF }-->
  
    <!--{ TMPL_LOOP comments }-->
    
    <div style="margin-left:<!--{ TMPL_VAR depth }-->em;">
      <div class="comment-block">
  
	<!--{ TMPL_IF deleted-p }-->
          <h4 id="comment:<!--{ TMPL_VAR id }-->">
	    --- This comment has been deleted ---
	  </h4>
	<!--{ TMPL_ELSE }-->
          <h4 id="comment:<!--{ TMPL_VAR id }-->"><!--{ TMPL_VAR title }--></h4>
        
          <!--{ TMPL_IF show-user-menu }-->
            <div class="authenticated post-menu">
              <ul>
                <li>
                  <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/archive/<!--{ TMPL_VAR year }-->/<!--{ TMPL_VAR month }-->/<!--{ TMPL_VAR date }-->/<!--{ TMPL_VAR post-id-url }-->/comments/<!--{ TMPL_VAR id }-->/delete"
                     title="Delete this comment">Delete</a>
                </li>
              </ul>
            </div>
          <!--{ /TMPL_IF }-->
  
          <div class="comment-header">
            Posted on <!--{ TMPL_VAR day-name }-->,
            <!--{ TMPL_VAR month-name }--> <!--{ TMPL_VAR date }-->,
            <!--{ TMPL_VAR year }-->
            at <!--{ TMPL_VAR hour }-->:<!--{ TMPL_VAR minute }-->
            
            <br />
            
            by <strong><!--{ TMPL_VAR author }--></strong>
            
            <!--{ TMPL_IF email }-->
              &nbsp;
              <span class="address"
                    title="Email address (cut'n'paste, remove spaces and replace &quot;(at)&quot; with &quot;@&quot;, and &quot;(dot)&quot; with &quot;.&quot;)">&lt; <!--{ TMPL_VAR email }--> &gt;</span>
            <!--{ /TMPL_IF }-->
            
            <!--{ TMPL_IF url }-->
              <br />
              Website:
              <span class="address"
                    title="Website URL (cut'n'paste)"><!--{ TMPL_VAR url }--></span>
            <!--{ /TMPL_IF }-->
    
          </div>
        
          <p>
            <!--{ TMPL_VAR body }-->
          </p>
    
          <!--{ TMPL_IF show-reply-to-link-p }-->
            <span class="comment-footer">
              <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/archive/<!--{ TMPL_VAR year }-->/<!--{ TMPL_VAR month }-->/<!--{ TMPL_VAR date }-->/<!--{ TMPL_VAR post-id-url }-->/comments/<!--{ TMPL_VAR id }-->"
               title="Reply to comment &quot;<!--{ TMPL_VAR title }-->&quot;">Reply to this comment</a>
            </span>
          <!--{ /TMPL_IF }-->

        <!--{ /TMPL_IF }-->   
      </div>
    </div>
    <!--{ /TMPL_LOOP }-->
  
  <!--{ /TMPL_IF }-->

  <!--{ TMPL_INCLUDE "comment-form.tpl" }-->

</div>

<!-- End comments -->
