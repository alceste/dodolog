<!-- Start error page -->

<div class="post">     
  <div class="form">
    <div class="error-element">
      <p>
	<!--{ TMPL_VAR dodolog-error-message }-->.
      </p>
      <p>
	You can return to the
	<a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/"
	   title="Blog main page">main page</a>.
      </p>
    </div>
  </div>
</div>

<!-- End error page -->
