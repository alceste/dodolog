A new comment has been added to your post:

    "<!--{ TMPL_VAR post-title }-->"
    http://<!--{ TMPL_VAR dodolog-http-host }--><!--{ TMPL_VAR dodolog-http-path-prefix }-->/posts/<!--{ TMPL_VAR post-id-url }-->

Quick link to the comment:

    http://<!--{ TMPL_VAR dodolog-http-host }--><!--{ TMPL_VAR dodolog-http-path-prefix }-->/posts/<!--{ TMPL_VAR post-id-url }-->#comment:<!--{ TMPL_VAR comment-id }-->

Quick link for deleting it (requires authentication):

    http://<!--{ TMPL_VAR dodolog-http-host }--><!--{ TMPL_VAR dodolog-http-path-prefix }-->/posts/<!--{ TMPL_VAR post-id-url }-->/comments/<!--{ TMPL_VAR comment-id }-->/delete

Comment details follow.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

IP address: <!--{ TMPL_VAR comment-ip-address }-->
Author:     <!--{ TMPL_VAR comment-author }-->
Email:      <!--{ TMPL_VAR comment-email }-->
Website:    <!--{ TMPL_VAR comment-url }-->
Title:      <!--{ TMPL_VAR comment-title }-->

<!--{ TMPL_VAR comment-body }-->
