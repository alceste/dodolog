<!-- Start post preview form -->

<!--{ TMPL_LOOP posts }-->
  <div class="form">
    <form method="post" action="">
      <fieldset class="preview">
	<div class="form-buttons">
	    <input type="hidden" name="id"
		   id="form-id" value="<!--{ TMPL_VAR id-html }-->" />
	    
	    <input type="hidden" name="title"
		   id="form-title" value="<!--{ TMPL_VAR title-html }-->" />
	    
	    <input type="hidden" name="body"
		   id="form-body" value="<!--{ TMPL_VAR body-html }-->" />
	    
            <!--{ TMPL_LOOP selected-category-names }-->
              <input type="hidden"
		     name="category:<!--{ TMPL_VAR name-html }-->"
		     id="chk-<!--{ TMPL_VAR name-html }-->"
                     value="<!--{ TMPL_VAR name-html }-->" />
            <!--{ /TMPL_LOOP }-->

	    <input type="hidden" name="new-categories"
		   id="form-new-categories"
		   value="<!--{ TMPL_VAR new-categories-raw }-->" />
	    
	    <input type="submit" name="cancel" value="Cancel"
		   title="Cancel publication" />
	    &nbsp;&nbsp;&nbsp;&nbsp;
	    <input type="submit" name="publish" value="Publish"
		   title="Publish your post" />
	    <input type="submit" name="edit" value="Edit"
		   title="Continue editing your post" />
	</div>
	
      </fieldset>
    </form>
  </div>
<!--{ /TMPL_LOOP }-->

<!-- End post preview form -->
