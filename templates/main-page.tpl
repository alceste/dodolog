<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title><!--{ TMPL_VAR dodolog-blog-title-html }--></title>
    
    <!-- Meta Tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!--{ TMPL_IF dodolog-override-keywords }-->
      <meta name="keywords"
	    content="<!--{ TMPL_VAR dodolog-override-keywords }-->" />
    <!--{ TMPL_ELSE }-->
      <meta name="keywords"
	    content="<!--{ TMPL_VAR dodolog-blog-keywords-html }-->" />
    <!--{ /TMPL_IF }-->

    <meta name="description"
	  content="<!--{ TMPL_VAR dodolog-blog-description-html }-->" />
    <meta name="author"
	  content="<!--{ TMPL_VAR dodolog-blog-author-html }-->" />

    <!--Style Sheets -->
    <link rel="stylesheet" type="text/css" media="screen"
	  href="/static/style.css" />
    
  </head>


  <body>
    <!-- Shadows -->
    <div class="s1"><div class="s2"><div class="s3"><div class="s4"><div class="s5"><div class="s6"><div class="s7"><div class="s8">

      <!-- Begin Header -->
      <div id="header">
          <h1 id="title">
	    <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->"
	       title="Go to main page">
	      <!--{ TMPL_VAR dodolog-blog-title-html }-->
	    </a>
	  </h1>
	  <p id="description">
	    <!--{ TMPL_VAR dodolog-blog-description-html }-->
	  </p>
      </div>

      <!-- End Header -->

      <div id="content">

	<div id="main">


          <!--{TMPL_IF dodolog-include-sidebar }-->
            <!--{ TMPL_INCLUDE sidebar.tpl }-->
          <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-include-post-preview }-->
            <div class="preview">
              <em>Post preview</em>
            </div>
          <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-archive-title }-->
            <div class="archive">
              <em>
		<!--{ TMPL_VAR dodolog-archive-title }-->
	      </em>

	      <!--{ TMPL_IF dodolog-batch-pages }-->
	        <!--{ TMPL_INCLUDE "batch-navigation.tpl" }-->
	      <!--{ /TMPL_IF }-->
            </div>
          <!--{ /TMPL_IF }-->



	  <!--{ TMPL_IF dodolog-include-posts }-->
	    <!--{ TMPL_INCLUDE "posts.tpl" }-->
	  <!--{ /TMPL_IF }-->

	  <!--{ TMPL_IF dodolog-include-edit-post }-->
	    <!--{ TMPL_INCLUDE "edit-post.tpl" }-->
	  <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-include-login }-->
            <!--{ TMPL_INCLUDE "login.tpl" }-->
          <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-include-logout }-->
            <!--{ TMPL_INCLUDE "logout.tpl" }-->
          <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-include-error }-->
            <!--{ TMPL_INCLUDE "error.tpl" }-->
          <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-include-delete-post }-->
            <!--{ TMPL_INCLUDE "delete-post.tpl" }-->
          <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-include-comment-reply }-->
            <!--{ TMPL_INCLUDE "comments.tpl" }-->
          <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-include-delete-comment }-->
            <!--{ TMPL_INCLUDE "delete-comment.tpl" }-->
          <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-include-post-preview }-->
	      <!--{ TMPL_INCLUDE "post-preview-form.tpl" }-->
          <!--{ /TMPL_IF }-->

          <!--{ TMPL_IF dodolog-archive-title }-->
            <div class="archive force-footer">
              <em>
		<!--{ TMPL_VAR dodolog-archive-title }-->
	      </em>

	      <!--{ TMPL_IF dodolog-batch-pages }-->
	        <!--{ TMPL_INCLUDE "batch-navigation.tpl" }-->
	      <!--{ /TMPL_IF }-->
            </div>
          <!--{ /TMPL_IF }-->

	</div>

      </div>

      <!-- Begin footer -->
      <div id="footer">
  	<p>
          <!--{ TMPL_IF dodolog-auth-user-nick }-->
	    <!-- Already logged in, no "log in" link -->
          <!--{ TMPL_ELSE }-->
            <!--{ TMPL_IF dodolog-include-login-link }-->
              <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/login"
	         title="Log in">
	        Log in
	      </a>
            <!--{ /TMPL_IF }-->
	  <!--{ /TMPL_IF }-->
	</p>
      </div>
      <!-- End footer -->
      
    </div></div></div></div></div></div></div></div>
    <!-- End shadows -->
    
  </body>
</html>
