<!-- Start logout page -->

<div class="post">     
  <div class="form">
    <div class="login-form">
      <!--{ TMPL_IF dodolog-auth-user-nick }-->
        <form method="post" action="">
	  <fieldset>
	    <legend>Logging out</legend>
	    
	    <p>
	      Close the session for user
	      &quot;<!--{ TMPL_VAR dodolog-auth-user-nick }-->&quot;?
	    </p>

	    <div class="form-buttons">
	      <p>
		<input type="submit" name="cancel" value="Cancel"
		       title="Cancel request" />
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="logout" value="Ok"
		       title="Proceed with log out" />
	      </p>
	    </div>
	  </fieldset>
	</form>
      <!--{ TMPL_ELSE }-->
        <p>
	  You have been logged out.
        </p>
        <p>
	  You can return to the
	  <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/"
	     title="Blog main page">main page</a>.
	</p>
      <!--{ /TMPL_IF }-->
    </div>
  </div>
</div>

<!-- End logout page -->
