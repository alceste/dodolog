<!-- Start login page -->

<div class="post">     
  <div class="form">
    <div class="login-form">
      <form method="post" action="">
	<fieldset>
	  <legend>Authentication</legend>
	  
          <!--{ TMPL_IF dodolog-error }-->
            <div class="error">
	      <!--{ TMPL_VAR dodolog-error }-->
	    </div>
          <!--{ /TMPL_IF }-->

	  <p>
	    <label for="form-id">Nick</label>
	    <br />
            <!--{ TMPL_IF errors-user-nick }-->
              <div class="error">
		<!--{ TMPL_VAR errors-user-nick }-->
              </div>
            <!--{ /TMPL_IF }-->
	    <input type="text" class="form" name="user-nick"
		   id="form-user-nick" title="Username for logging in"
		   value="<!--{ TMPL_VAR user-nick }-->"/>
	  </p>
	  
	  <p>
	    <label for="form-title">Password</label>
	    <br />
            <!--{ TMPL_IF errors-password }-->
              <div class="error">
		<!--{ TMPL_VAR errors-password }-->
              </div>
            <!--{ /TMPL_IF }-->
	    <input type="password" class="form" name="password"
		   id="form-password" title="Your password" value=""/>
	  </p>
	  
	  <div class="form-buttons">
	    <p>
	      <!--{ TMPL_IF came-from }-->
	        <input type="hidden" name="came-from"
		       value="<!--{ TMPL_VAR came-from }-->" />
	      <!--{ /TMPL_IF }-->
	      <input type="submit" name="login" value="Log in"
		     title="Proceed with authentication" />
	      &nbsp;&nbsp;&nbsp;&nbsp;
	      <input type="submit" name="cancel" value="Cancel"
		     title="Cancel authentication" />
	    </p>
	  </div>
	</fieldset>
      </form>
    </div>
  </div>
</div>

<!-- End login page -->
