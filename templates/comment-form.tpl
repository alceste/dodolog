<!-- Start comment form -->

<div class="form">
  <form id="comment-form" method="post" action="">
    <fieldset>
        <!--{ TMPL_IF reply-to-comment-id }-->
          <legend>Reply to comment</legend>
        <!--{ TMPL_ELSE }-->
          <legend>Add a comment</legend>
        <!--{ /TMPL_IF }-->

	<p>
	  <label for="form-author">Author (optional)</label>
	  <br />
          <!--{ TMPL_IF errors-comment-author }-->
            <div class="error">
              <!--{ TMPL_VAR errors-comment-author }-->
            </div>
          <!--{ /TMPL_IF }-->

	  <input type="text" class="form" name="comment-author"
		 title="Your name, nick, pseudonym..."
		 id="form-author" value="<!--{ TMPL_VAR comment-author }-->" />
	</p>

	<p>
	  <label for="form-email">Email address (optional). It will be shown in garbled form, to make spammers' life harder</label>
	  <br />
          <!--{ TMPL_IF errors-comment-email }-->
            <div class="error">
              <!--{ TMPL_VAR errors-comment-email }-->
            </div>
          <!--{ /TMPL_IF }-->

	  <input type="text" class="form" name="comment-email"
		 title="Your email address"
		 id="form-email" value="<!--{ TMPL_VAR comment-email }-->" />
	</p>

	<p>
	  <label for="form-url">Website URL (optional)</label>
	  <br />
          <!--{ TMPL_IF errors-comment-url }-->
            <div class="error">
              <!--{ TMPL_VAR errors-comment-url }-->
            </div>
          <!--{ /TMPL_IF }-->

	  <input type="text" class="form" name="comment-url"
		 title="Your home page, blog, company website..."
		 id="form-url" value="<!--{ TMPL_VAR comment-url }-->" />
	</p>

	<p>
	  <label for="form-title">Title (optional)</label>
	  <br />
          <!--{ TMPL_IF errors-comment-title }-->
            <div class="error">
              <!--{ TMPL_VAR errors-comment-title }-->
            </div>
          <!--{ /TMPL_IF }-->

	  <input type="text" class="form" name="comment-title"
		 title="The title of your comment"
		 id="form-title" value="<!--{ TMPL_VAR comment-title }-->" />
	</p>
	
	<p>
	  <label for="form-body">Body (maximum size: 8192 characters)</label>
	  <br/>

          <!--{ TMPL_IF errors-comment-body }-->
            <div class="error">
              <!--{ TMPL_VAR errors-comment-body }-->
            </div>
          <!--{ /TMPL_IF }-->

	  <textarea name="comment-body" id="form-body" rows="25" cols="80"
		    title="Body of your comment. No markup is allowed, but paragraphs will be kept"
		    ><!--{ TMPL_VAR comment-body }--></textarea>
	</p>

	<div class="form-buttons">
	  <p>
	    <input type="submit" name="publish-comment" value="Publish"
		   title="Publish your comment" />
	  </p>
	</div>
	
    </fieldset>
  </form>
</div>

<!-- End comment form -->
