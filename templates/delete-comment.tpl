<!-- Start logout page -->

<div class="post">     
  <div class="form">
    <div class="login-form">
      <!--{ TMPL_IF comment-not-yet-deleted-p }-->
        <form method="post" action="">
	  <fieldset>
	    <legend>Deleting comment</legend>
	    
	    <p>
	      Really delete comment
	      <strong>&quot;<!--{ TMPL_VAR comment-title-html }-->&quot;</strong>
	      for post
	      <strong>&quot;<!--{ TMPL_VAR post-title-html }-->&quot;</strong>?
	    </p>

            <!--{ TMPL_IF comment-has-replies-p }-->
	      <br /> <br />
	      <fieldset>
		<legend>Replies</legend>

		<p>
                  The comment has got some replies.  Choose an action:
		</p>
		
		<p>
		  <input id="empty-comment"
			 type="radio" name="replies-action" value="empty-comment"
			 checked="1" />
		  <label class="plain" for="reparent-replies">
		    Leave an empty comment with replies
		  </label>
		</p>

		<p>
		  <input id="reparent-replies"
			 type="radio" name="replies-action" value="reparent" />
		  <label class="plain" for="reparent-replies">
		    Delete the comment and reparent replies
		  </label>
		</p>
		
		<p>
		  <input id="cascade replies"
			 type="radio" name="replies-action" value="cascade" />
		  <label class="plain" for="cascade-replies">
		    Delete both the comment and its replies
		  </label>
		</p>
	      </fieldset>
	      <br /> <br />
            <!--{ /TMPL_IF }-->

	    <div class="form-buttons">
	      <p>
		<input type="submit" name="cancel" value="Cancel"
		       title="Cancel request" />
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="logout" value="Ok"
		       title="Proceed with deletion" />
	      </p>
	    </div>
	  </fieldset>
	</form>
      <!--{ TMPL_ELSE }-->
        <p>
	  Comment
	  <strong>&quot;<!--{ TMPL_VAR comment-title-html }-->&quot;</strong>
	  for post
	  <strong>&quot;<!--{ TMPL_VAR post-title-html }-->&quot;</strong>
	  has been deleted.
        </p>
        <p>
	  You can return to the
	  <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/archive/<!--{ TMPL_VAR year }-->/<!--{ TMPL_VAR month }-->/<!--{ TMPL_VAR date }-->/<!--{ TMPL_VAR post-id-url }-->">post</a>.
	</p>
      <!--{ /TMPL_IF }-->
    </div>
  </div>
</div>

<!-- End logout page -->
