<!-- Start logout page -->

<div class="post">     
  <div class="form">
    <div class="login-form">
      <!--{ TMPL_IF post-not-yet-deleted }-->
        <form method="post" action="">
	  <fieldset>
	    <legend>Deleting post</legend>
	    
	    <p>
	      Really delete post
	      <strong>&quot;<!--{ TMPL_VAR post-title }-->&quot;?</strong>
	    </p>

	    <div class="form-buttons">
	      <p>
		<input type="submit" name="cancel" value="Cancel"
		       title="Cancel request" />
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="logout" value="Ok"
		       title="Proceed with deletion" />
	      </p>
	    </div>
	  </fieldset>
	</form>
      <!--{ TMPL_ELSE }-->
        <p>
	  Post <strong>&quot;<!--{ TMPL_VAR post-title }-->&quot;</strong>
	  has been deleted.
        </p>
        <p>
	  You can return to the
	  <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/"
	     title="Blog main page">main page</a>.
	</p>
      <!--{ /TMPL_IF }-->
    </div>
  </div>
</div>

<!-- End logout page -->
