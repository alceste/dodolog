<!-- Start archive index -->

<div id="archive-page-index">
  <h2>On this page</h2>
  <ul>
    <!--{ TMPL_LOOP posts }-->
    <li>
      <a href="#post:<!--{ TMPL_VAR id-url }-->"
	 title="Jump to &quot;<!--{ TMPL_VAR title-html }-->&quot;">
	<!--{ TMPL_VAR title-html }-->
      </a>
    </li>
    <!--{ /TMPL_LOOP }-->
  </ul>
</div>

<!-- End archive index -->
