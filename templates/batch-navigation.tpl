<!-- Start batch navigation element -->

<div class="batch-navigation">
  <!--{ TMPL_IF dodolog-prev-batch }-->
    <a href="?start=<!--{ TMPL_VAR dodolog-prev-batch }-->"
       title="Previous page">&lt;&lt;&nbsp;</a>
  <!--{ TMPL_ELSE }-->
    <span title="No previous page">&lt;&lt;&nbsp;</span>
  <!--{ /TMPL_IF }-->

  <!--{ TMPL_LOOP dodolog-batch-pages }-->
    <!--{ TMPL_IF not-current }-->
      <a href="?start=<!--{ TMPL_VAR start }-->"
	 title="Go to page <!--{ TMPL_VAR number }-->"><!--{ TMPL_VAR number }--></a>
    <!--{ TMPL_ELSE }-->
        <span title="Current page: <!--{ TMPL_VAR number }-->">
	  [<!--{ TMPL_VAR number }-->]
	</span>
    <!--{ /TMPL_IF }-->
  <!--{ /TMPL_LOOP }-->

  <!--{ TMPL_IF dodolog-next-batch }-->
    <a href="?start=<!--{ TMPL_VAR dodolog-next-batch }-->"
       title="Next page">&gt;&gt;&nbsp;</a>
  <!--{ TMPL_ELSE }-->
    <span title="No more pages">&gt;&gt;&nbsp;</next>
  <!--{ /TMPL_IF }-->

</div>

<!-- End batch navigation element -->
