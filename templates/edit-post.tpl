<!-- Start edit post form -->

<div class="post">     
  <div class="form">
    <form method="post" action="">
      <fieldset>
	<legend>Publish new post</legend>

	<p>
	  <label for="form-id">ID (will appear in the URL)</label>
	  <br />
          <!--{ TMPL_IF errors-id }-->
            <div class="error">
              <!--{ TMPL_VAR errors-id }-->
            </div>
          <!--{ /TMPL_IF }-->
	  <input type="text" class="form" name="id"
		 id="form-id"
		 title="It should be easy to type in a URL (short, no spaces or special characters, etc.)"
		 value="<!--{ TMPL_VAR id-html }-->" />
	</p>

	<p>
	  <label for="form-title">Title</label>
	  <br />
          <!--{ TMPL_IF errors-title }-->
            <div class="error">
              <!--{ TMPL_VAR errors-title }-->
            </div>
          <!--{ /TMPL_IF }-->
	  <input type="text" class="form" name="title"
		 title="The title of your post"
		 id="form-title" value="<!--{ TMPL_VAR title-html }-->" />
	</p>
	
	<p>
	  <label for="form-body">Body</label>
	  <br/>

	  <script  type="text/javascript"
		   src="/static/tiny_mce/tiny_mce.js">
	  </script>
	  <script  type="text/javascript">
	    tinyMCE.init({
	        mode     : "specific_textareas",
                elements : "form-body",
	        theme    : "advanced"
	    });
	  </script>

          <!--{ TMPL_IF errors-body }-->
            <div class="error">
              <!--{ TMPL_VAR errors-body }-->
            </div>
          <!--{ /TMPL_IF }-->
	  <textarea mce_editable="true" name="body"
		    id="form-body" rows="25" cols="80"
		    title="Body of your post"
		    ><!--{ TMPL_VAR body-html }--></textarea>
	</p>

	<fieldset>
	  <legend>Categories</legend>
          <!--{ TMPL_IF errors-categories }-->
            <div class="error">
              <!--{ TMPL_VAR errors-categories }-->
            </div>
          <!--{ /TMPL_IF }-->

	  <div style="width:50%; float:left">
            <!--{ TMPL_LOOP categories }-->
              <input type="checkbox"
		     name="category:<!--{ TMPL_VAR name-html }-->"
		     id="chk-<!--{ TMPL_VAR name-html }-->"
		     value="<!--{ TMPL_VAR name-html }-->"
		     <!--{ TMPL_IF selected-p }-->
                       checked="1"
                     <!--{ /TMPL_IF }-->
		     title="Select category &quot;<!--{ TMPL_VAR name-html }-->&quot;"
		     />
	      <label class="checkbox" for="chk-<!--{ TMPL_VAR name-html }-->">
		&nbsp;<!--{ TMPL_VAR name-html }-->
	      </label>
	      <br />
            <!--{ /TMPL_LOOP }-->
	  </div>

          <div style="width:50%; float:right">
	    <label for="form-new-categories">
	      New categories (one per line)
	    </label>
	    <textarea id="form-new-categories"
		      rows="5" cols="20"
		      title="Just if the predefined ones don't fit. You shouldn't use spaces nor special characters, since categories may appear in URLs"
		      name="new-categories"
		      ><!--{ TMPL_VAR new-categories-raw }--></textarea>
	  </div>
	</fieldset>

	<div class="form-buttons">
	  <p>
	    <input type="submit" name="preview" value="Preview"
		   title="Preview your post" />
	    <input type="submit" name="publish" value="Publish"
		   title="Publish your post" />
	    &nbsp;&nbsp;&nbsp;&nbsp;
	    <input type="submit" name="cancel" value="Cancel"
		   title="Cancel publication" />
	  </p>
	</div>
	
      </fieldset>
    </form>
  </div>
</div>

<!-- End edit post form -->
