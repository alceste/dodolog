<!-- Start posts -->

<div class="post-block-container">
 <div class="post-block">

  <!--{ TMPL_LOOP posts }-->

    <h2 id="post:<!--{ TMPL_VAR id-html }-->" 
       class="date-header">
      <!--{ TMPL_VAR day-name }-->,
      <!--{ TMPL_VAR month-name }--> <!--{ TMPL_VAR date }-->,
      <!--{ TMPL_VAR year }-->
    </h2>
    <div class="post">	   
      <h3 class="post-title">
	<!--{ TMPL_VAR title-html }-->
      </h3>
      
      <div class="post-body">
	<!--{ TMPL_IF show-user-menu-p }-->
	  <div class="authenticated post-menu">
	    <ul>
	      <li>
		<a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/archive/<!--{ TMPL_VAR year }-->/<!--{ TMPL_VAR month }-->/<!--{ TMPL_VAR date }-->/<!--{ TMPL_VAR id-url }-->/edit"
		   title="Edit or update this post">Edit</a>
	      </li>
	      <li>
		<a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/archive/<!--{ TMPL_VAR year }-->/<!--{ TMPL_VAR month }-->/<!--{ TMPL_VAR date }-->/<!--{ TMPL_VAR id-url }-->/delete"
		   title="Delete this post">Delete</a>
	      </li>
	    </ul>
	  </div>
	<!--{ /TMPL_IF }-->
  
	<!--{ TMPL_VAR body }-->
      </div>
      
      <p class="post-footer">
	<em>
	  posted by
	  <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/user/<!--{ TMPL_VAR user-nick-url }-->"
	     title="Archive and profile for user &quot;<!--{ TMPL_VAR user-nick-html }-->&quot;"
	     ><!--{ TMPL_VAR user-nick-html }--></a>
	  on
	  <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/archive/<!--{ TMPL_VAR year }-->/<!--{ TMPL_VAR month }-->/<!--{ TMPL_VAR date }-->/<!--{ TMPL_VAR id-url }-->"
	     title="Permanent link"><!--{ TMPL_VAR day-name }-->,
	  <!--{ TMPL_VAR month-name }--> <!--{ TMPL_VAR date }-->,
	  <!--{ TMPL_VAR year }-->
	  at <!--{ TMPL_VAR hour }-->:<!--{ TMPL_VAR minute }--></a>
	</em>

        <!--{ TMPL_IF show-comments-count-in-footer-p }-->
  	  <a class="comment-link" title="Add or read comments"
	     href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/archive/<!--{ TMPL_VAR year }-->/<!--{ TMPL_VAR month }-->/<!--{ TMPL_VAR date }-->/<!--{ TMPL_VAR id-url }-->#comments">
	    (<!--{ TMPL_VAR comments-count }--> comments)</a>
        <!--{ /TMPL_IF }-->
	<br />
  
	<em>
	  Filed under:
	  <!--{ TMPL_LOOP categories }-->
	    <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/categories/<!--{ TMPL_VAR name-url }-->"
	       title="Archive for category &quot;<!--{ TMPL_VAR name-html }-->&quot;"><!--{ TMPL_VAR name-html }--></a>
	  <!--{ /TMPL_LOOP }-->
	</em>
      </p>
    </div>
  
    <!--{ TMPL_IF with-comments-p }-->
      <!--{ TMPL_INCLUDE "comments.tpl" }-->
    <!--{ /TMPL_IF }-->

  <!--{ /TMPL_LOOP }-->

 </div>
</div>
 
<!-- End posts -->
