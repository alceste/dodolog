<!-- Begin sidebar -->
<div id="sidebar">

  <!--{ TMPL_IF dodolog-auth-user-nick }-->
    <div id="user-menu" class="authenticated">
      <h2>User Menu for <!--{ TMPL_VAR dodolog-auth-user-nick }--></h2>

      <ul class="clean">
        <li>
	  <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/new-post"
	     title="Publish new post">Publish new post</a>
        </li>
      </ul>

      <form method="post"
	    action="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/logout">
	<div class="form-buttons">
	  <input type="hidden" name="form.submitted" value="1" />
	  
	  <input type="submit" name="logout" value="Log out"
		 title="Close session" />
	</div>
      </form>
    </div>
  <!--{ /TMPL_IF }-->

  <!--{ TMPL_IF dodolog-archive-title }-->
    <!--{ TMPL_INCLUDE "archive-page-index.tpl" }-->
  <!--{ /TMPL_IF }-->

  <h2 class="sidebar-title">Last Posts</h2>
  <ul>
    <!--{ TMPL_LOOP dodolog-last-posts }-->
    <li>
      <a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->#post:<!--{ TMPL_VAR id-url }-->"
	 title="Jump to &quot;<!--{ TMPL_VAR title-html }-->&quot;">
	<!--{ TMPL_VAR title-html }-->
      </a>
    </li>
    <!--{ /TMPL_LOOP }-->
  </ul>
  

  <h2 class="sidebar-title">Archives</h2>
  <ul>
    <!--{ TMPL_LOOP dodolog-archive-dates }-->
      <li>
	<a href="<!--{ TMPL_VAR dodolog-http-path-prefix }-->/archive/<!--{ TMPL_VAR year }-->/<!--{ TMPL_VAR month }-->"
	   title="Archive for <!--{ TMPL_VAR month-name }--> <!--{ TMPL_VAR year }-->">
	  <!--{ TMPL_VAR month-name }-->
	  <!--{ TMPL_VAR year }-->
        </a>
      </li>
    <!--{ /TMPL_LOOP }-->    
  </ul>
  
  
  <h2 class="sidebar-title">Links</h2>
  <ul>

    <li><a href="http://www.alistapart.com">ALA</a></li>

    <li><a href="http://www.oswd.org">OSWD</a></li>

    <li><a href="http://www.w3schools.com">W3schools</a></li>

  </ul>

  <!-- imgbox -->
  <div id="lisp-logo">
    <a href="http://lisperati.com/logo.html"
       title="Made with secret alien technology: Lisp">
      <img src="/static/img/lisp-logo.png" width="128"
	   height="111"
	   alt="dodolog" />
    </a>
  </div>
  
  <div id="imgbox">
    <a href="http://muvara.org/software/dololog"
       title="Powered by dodolog">
      <img src="/static/img/dodolog.png" width="88"
	   height="31"
	   alt="dodolog" />
    </a>

    <a href="http://www.cons.org/cmucl"
       title="Powered by CMU Common Lisp">
      <img src="/static/img/cmucl.png" width="88"
	   height="31"
	   alt="CMUCL" />
    </a>

    <a href="http://www.postgresql.org/"
       title="Powered by PostgreSQL">
      <img src="/static/img/postgresql.png" width="88"
	   height="31"
	   alt="PostgreSQL" />
    </a>

    <a href="http://www.gnu.org/"
       title="Powered by GNU">
      <img src="/static/img/gnu.png" width="88"
	   height="31"
	   alt="GNU is Not Unix" />
    </a>

    <a href="http://www.gnu.org/software/emacs/emacs.html"
       title="Created with GNU Emacs">
      <img src="/static/img/emacs.png" width="88"
	   height="31"
	   alt="GNU Emacs" />
    </a>

    <a href="http://www.ubuntulinux.com/"
       title="Developed on Ubuntu GNU/Linux">
      <img src="/static/img/ubuntu.png" width="88"
	   height="31"
	   alt="Ubuntu GNU/Linux" />
    </a>

    <a href="http://getfirefox.com/"
       title="Get Firefox - The Browser, Reloaded.">
      <img src="http://www.mozilla.org/products/firefox/buttons/getfirefox_88x31.png"
	   width="88" height="31"
	   alt="Get Firefox" />
    </a> 

    <a href="http://validator.w3.org/check?uri=referer"
       title="W3C Validation Service">
      <img src="http://www.w3.org/Icons/valid-xhtml10"
	   alt="Valid XHTML 1.0 Strict"
	   height="31" width="88" />
    </a>
  </div>
  <!-- End imgbox -->

</div>
<!-- End sidebar -->
