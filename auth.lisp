;;;; dodolog, aka yet another Common LISP blog
;;;;
;;;; Copyright (C) 2005 by Alceste Scalas <alceste@muvara.org>
;;;;
;;;; This program is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation; either version 2 of
;;;; the License, or (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public
;;;; License along with this program; if not, write to the Free
;;;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;;;; MA 02111-1307, USA.

(in-package #:dodolog)

;;; FIXME: cl-base64 doesn't define usb8-array-to-base64-str... uh?
(defparameter *dodolog-base64-encode-usb8-array-to-str*
  (cl-base64::def-*-to-base64-* :usb8-array :string)
  "Encode an unsigned-byte 8 array to a string")

(defun auth-get-user (user-nick password)
  "Get the user with the given nick and password, or nil if it doesn't exist"
    (db-get-user-with-password user-nick
			       (-encrypt-password user-nick password)))

(defun auth-login-user (user)
  "Log the specified user into the system"
  (start-session)
  (setf (session-value :authenticated-user *session*) user))

(defun auth-logout-user ()
  "Log out the currently authenticated user (if any)"
  (delete-session-value :authenticated-user *session*))

(defun auth-get-authenticated-user ()
  "Get the currently authenticated user, or nil"
  (when *session*
    (session-value :authenticated-user *session*)))

(defun auth-get-authenticated-nick ()
  "Get the currently authenticated user nickname, or nil"
  (when *session*
    (nick (session-value :authenticated-user *session*))))

(defun auth-anonymous-user-p ()
  "Return t if the current user is not authenticated"
  (not (auth-get-authenticated-user)))

(defmacro with-authenticated-user ((user) &body body)
  "Execute the specified block with the user variable bound to the
authenticated user.  Redirect to the login page if the visitor is anonymous"
  `(let ((,user (auth-get-authenticated-user)))
     (if (not ,user)
	 (-redirect-to-authentication-page)
	 (progn ,@body))))

(defmacro with-authenticated-user-post ((post year month day post-id)
					&body body)
  "Execute a body of code with the specified post variable bound
to the authenticated user's post identified by post-id.  Redirect
to the login page if the visitor is anonymous, or if the
specified post-id is owned by someone else.  Show 404 page if the
post-id does not exist"
  (let ((user (gensym)))
    `(with-authenticated-user (,user)
       (with-post (,post ,year ,month ,day ,post-id)
	 (if (or (not ,user) (not (string-equal (user-nick ,post)
						(nick ,user))))
	     (-redirect-to-authentication-page)
	     (progn ,@body))))))

(defun -encrypt-password (user-nick password) ; FIXME: support Unicode strings!
  "Return an encrypted password from the given nick and cleartext password"
  ;; Password encryption scheme: (base64 (sha1 (+ username password)))
  (let* ((digest (ironclad:make-digest :sha1))) ; FIXME: sha256 seems buggy
    (funcall *dodolog-base64-encode-usb8-array-to-str*
	     (ironclad:produce-digest
	      (ironclad:update-digest digest
				      (kmrcl:string-to-usb8-array
				       (concatenate 'string
						    user-nick password)))))))

(defun -redirect-to-authentication-page ()
  "Function name says all :-)"
  (redirect (concatenate 'string *dodolog-http-path-prefix* "/login"
			 "?came-from="
			 (url-encode (request-uri *request*)))))
